const http = require("http");
var fs = require("fs");
require("app-module-path").addPath(__dirname);
const app = require("./app/index");
const port = 8443;

// const options = {
//   key: fs.readFileSync("/etc/nginx/ssl/backend/kampon.key"),
//   cert: fs.readFileSync("/etc/nginx/ssl/backend/kampon.crt")
// };

const server = http.createServer(app);

server.listen(port, function(err) {
  if (err) {
    console.log({ err });
  } else {
    console.log("listening on port " + port);
  }
});
