const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkMemberToken");
const Cart = require("app/models/cart");

router.get("/:discountId", checkToken, async (req, res) => {
  console.log("=================== Member Check Cart ================");
  const userId = req.tokenInfo._id;
  const { discountId } = req.params;
  const resCheckCart = await Cart.findOne({
    cartBuyer: userId,
    cartDiscount: discountId
  });
  if (resCheckCart) {
    res.status(200).json({ CODE: 2057 });
  } else {
    res.status(500).json({ CODE: 1032 });
  }
});

module.exports = router;

//CODE: 2057 ==> this discount exists in the cart
//CODE: 1032 ==> doesnt exist
