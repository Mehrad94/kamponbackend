const express = require("express");
const router = express.Router();
const Discount = require("../models/discount");
//------------------------------------------- Get Order by member id
router.post("/", (req, res) => {
  console.log("======= Discount Search =======");
  const searchString = req.body.searchString;
  console.log(searchString);
  const regex = new RegExp(escapeRegex(searchString), "gi");
  Discount.find({ isActive: true, disTitle: regex })
    .exec()
    .then(discounts => res.status(200).json({ discounts }))
    .catch(err => res.status(500).json({ err }));
});

function escapeRegex(text) {
  if (text) {
    return text.toString().replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }
}

module.exports = router;
