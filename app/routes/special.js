const express = require("express");
const router = express.Router();
const discountModal = require("../models/discount");
const {
  MOST_DISCOUNT,
  POPULAR,
  NEWEST,
  OUR_OFFER,
  MOST_SOLD
} = require("app/values/strings");

router.get("/:type/:page", async (req, res, next) => {
  const type = req.params.type;
  const page = req.params.page;
  console.log({ type, page });
  switch (type) {
    case MOST_DISCOUNT:
      const resGetMostDiscount = await discountModal.paginate(
        { isActive: true },
        { limit: 4, page, sort: { disPercent: -1 } }
      );
      res.status(200).json(resGetMostDiscount);
      break;
    case POPULAR:
      const resGetPopular = await discountModal.paginate(
        { isActive: true },
        { limit: 4, page, sort: { viewCount: -1 } }
      );
      res.status(200).json(resGetPopular);
      break;
    case NEWEST:
      const resGetNewestDiscount = await discountModal.paginate(
        { isActive: true },
        { limit: 4, page, sort: { createdAt: -1 } }
      );
      res.status(200).json(resGetNewestDiscount);
      break;
    case MOST_SOLD:
      const resGetMostSold = await discountModal.paginate(
        { isActive: true },
        { limit: 4, page, sort: { boughtCount: -1 } }
      );
      res.status(200).json(resGetMostSold);
      break;
    default:
      return res.status(500).json({ msg: "bad string" });
  }
});

module.exports = router;
