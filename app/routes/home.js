const express = require("express");
const router = express.Router();
const categoriesModal = require("app/models/category");
const slidesModal = require("app/models/slide");
const discountModal = require("app/models/discount");
const callPython = require("./../constants/callPython");

router.get("/", async (req, res) => {
  console.log("\n======================== Home ==========================");

  //call python to get all slides with slideId Lable
  callPython("getSliders.py", "main", async result => {
    let firstPageInfo = { slides: [], cats: [], discounts: [] };

    //parse the retrieved json string and set it to firstPageInfo.slides
    firstPageInfo.slides = JSON.parse(result);

    callPython("getCategories.py", "", async result => {
      const cats = JSON.parse(result).filter(catItem => {
        if (catItem.activeCount > 0) {
          return true;
        } else {
          return false;
        }
      });

      console.log({ cats });

      //add categories to firstPageInfo.cats
      firstPageInfo.cats = cats;

      //get 4 discount of each category
      for (let i = 0; i < cats.length; i++) {
        //get discount
        let dis = await discountModal
          .find({ disCategory: cats[i].catTitle, isActive: true })
          .limit(4)
          .sort({ boughtCount: -1, viewCount: -1, createdAt: -1 });

        for (let j = 0; j < dis.length; j++) {
          dis[j] = { ...dis[j]._doc, catId: cats[i]._id };
        }

        //make array of arrays! like this [[dis1, dis2, dis3, dis4], [dis1, dis2, dis3, dis4], [dis1, dis2, dis3, dis4], [dis1, dis2, dis3, dis4]]
        if (dis.length > 0) firstPageInfo.discounts.push(dis);
      }

      //send te final result to the client
      res.status(200).json(firstPageInfo);
    });
  });
});

module.exports = router;
