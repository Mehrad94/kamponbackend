const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkMemberToken");
const Cart = require("app/models/cart");
const Discount = require("app/models/discount");
const mongoose = require("mongoose");
const request = require("request-promise");
const strings = require("app/values/strings");
router.get("/", checkToken, async (req, res) => {
  console.log("=================== Member Get Bank Url ===================");

  //get _id from members token
  const { _id: cartBuyer } = req.tokenInfo;
  console.log({ cartBuyer });

  //get his cart items from database
  const resGetCart = await Cart.find({ cartBuyer }).populate("cartDiscount");
  console.log({ resGetCart });

  //if no item in cart send an error
  if (!resGetCart.length) return res.status(500).json({ CODE: 1047 });

  //check if request is ok
  for (let i = 0; i < resGetCart.length; i++) {
    const cartItem = resGetCart[i];

    //get requested discount
    const resGetDiscount = await Discount.findOne({
      _id: mongoose.Types.ObjectId(cartItem.cartDiscount._id)
    });

    //check if requested discount is available
    if (!resGetDiscount || !resGetDiscount.isActive) {
      console.log({ CODE: strings.BAD_REQUEST, msg: "Invalid Discount" });
      return res
        .status(500)
        .json({ CODE: strings.BAD_REQUEST, msg: "Invalid Discount" });
    }
  }

  //calculate the total price
  let totalPrice = 0;
  resGetCart.map(cartItem => {
    totalPrice += cartItem.count * cartItem.cartDiscount.disNewPrice;
  });

  //log
  console.log({ totalPrice });

  //conectiong to bank
  let params = {
    MerchantID: "3510518c-ab28-11e9-a354-000c29344814",
    Amount: totalPrice.toString(),
    CallbackURL: "https://www.kampon.ir:8443/order",
    Description: "خرید کمپن"
  };

  let options = {
    method: "POST",
    uri: "https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json",
    headers: {
      "cache-control": "no-cache",
      "content-type": "aplication/json"
    },
    body: params,
    json: true
  };

  request(options)
    .then(async data => {
      if (data.Status === 100) {
        //add Authority to all items in this members cart
        const cartItem = await Cart.updateMany(
          {
            cartBuyer: mongoose.Types.ObjectId(cartBuyer)
          },
          { Authority: data.Authority }
        );

        //send the url to the client for payment
        res.send({
          URL: "https://www.zarinpal.com/pg/StartPay/" + data.Authority
        });
      }
    })
    .catch(error => {
      res.status(500).json({ CODE: 2966 });
    });
});

module.exports = router;

// CODE: 1019 ==> Conection to zarinpal failed
// CODE: 1093 ==> no cart with this ID
// CODE : 2966 ==> failed
