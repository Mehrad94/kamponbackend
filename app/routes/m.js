const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const security = require("app/constants/Security");

router.get("/", async (req, res) => {
  const { discountId, count, memberId } = req.body;
  // validate Id's
  const isDiscountIdValid = security.validateMongooseId(discountId);
  const isMemberIdValid = security.validateMongooseId(memberId);

  if (!isDiscountIdValid || !isMemberIdValid) {
    return res.status(500).json({ error: "Invalid Id" });
  }

  //check there is a user with this id or not
  const resFindMember = await Member.findOne({ _id: memberId });
  let flagCheckCart = false;
  if (resFindMember == null)
    return res.status(200).json({ msg: "Cannot find member" });

  //check there is a discount in cart or not
  const mCart = resFindMember.mCart;
  await mCart.map(cart => {
    if (discountId === cart.discountId) {
      cart.count = count;
      flagCheckCart = true;
    }
  });
  console.log("MAP DONE!");
  if (!flagCheckCart) {
    const newItem = { discountId, count };
    mCart.push(newItem);
  }
  const resUpdateCart = await Member.findOneAndUpdate(
    { _id: memberId },
    { $set: { mCart } },
    { new: true }
  );
  return res.status(200).json({ msg: "Cart updated" });
});
//------------------------------------------- Add new discount
router.post("/", (req, res) => {
  const { mName, mPassword, mPhoneNumber } = req.body;

  bcrypt.hash(mPassword, bcrypt.genSaltSync(15), (err, hash) => {
    if (err) {
      res.stat(500).json({ err });
    } else {
      const member = new Member({ mName, hash, mPhoneNumber });
      member
        .save()
        .then(res => {
          console.log({ res });
        })
        .catch(err => {
          console.log({ err });
        });
    }
  });
});

module.exports = router;
