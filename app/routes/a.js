const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const rp = require("request-promise");
const jwt = require("jsonwebtoken");
const securityConstants = require("app/constants/Security");

const API_KEY =
  "44503346776A7533366C6A652F783963704B477673427650684B6A39526A50363371357A6E38695865486F3D";

//------------------------------------------- Get token by phone number
router.post("/", async (req, res) => {
  if (req.body.mToken) {
    //--------------------------------------- Active Member
    const { mPhoneNumber, mToken } = req.body;
    console.log({ mPhoneNumber, mToken });
    const findMemberByPhoneNumber = await Member.findOne({ mPhoneNumber });
    if (findMemberByPhoneNumber == null)
      return res.status(500).json({ msg: "User not found" });

    if (findMemberByPhoneNumber.mToken != mToken)
      return res.status(500).json({ msg: "Token is false" });

    const token = jwt.sign(
      {
        mPhoneNumber,
        _id: findMemberByPhoneNumber._id,
        isAdmin: false,
        isOwner: false,
        isMember: true
      },
      securityConstants.tokensKey
    );

    const resActiveMember = await Member.findOneAndUpdate(
      { mPhoneNumber },
      { $set: { mIsActive: true, mToken: token } },
      { new: true }
    );
    if (resActiveMember == null)
      return res.status(500).json({ msg: "Please try again" });

    return res.status(200).json({ token });
  } else {
    //--------------------------------------- Make a new Member
    const mToken = Math.floor(Math.random() * 10000).toString();
    const mPhoneNumber = req.body.mPhoneNumber;

    //Check this user is exist or not
    const resFindUser = await Member.findOne({
      mPhoneNumber: req.body.mPhoneNumber
    });

    // User exist in Database
    if (resFindUser) {
      const resUpdateToken = await Member.findOneAndUpdate(
        { mPhoneNumber },
        { $set: { mToken } },
        { new: true }
      );
      if (resUpdateToken)
        return res.status(200).json({ token: mToken, isNewMember: false });
      else return res.status(500).json({ ER: "Some error accrued" });
    }

    // User not exist in Database
    else {
      const newMember = new Member({ mPhoneNumber, mToken });
      const saveNewMember = await newMember.save();
      if (saveNewMember)
        return res.status(200).json({ mToken, isNewMember: true });
      else return res.status(500).json({ ERR: "Cannot create Member" });
    }
  }
});

router.put("/", async (req, res) => {
  const { mName, mPhoneNumber } = req.body;

  if (!mName || !mPhoneNumber) {
    return res.status(500).json({ error: "invalid request" });
  }
  try {
    const resFindMember = await Member.findOneAndUpdate(
      { mPhoneNumber },
      { $set: { mName } },
      { new: true }
    );
    if (resFindMember) return res.status(202).json({ msg: "Member Updated" });
    res.status(404).json({ msg: "Member Not Found" });
  } catch (e) {
    res.status(500).json({ e });
  }
});
module.exports = router;
