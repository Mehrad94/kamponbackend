const express = require("express");
const router = express.Router();
const Order = require("../models/order");
const Discount = require("../models/discount");
//------------------------------------------- Get Order by member id
router.get("/:memberId", (req, res) => {
  const oMember = req.params.memberId;
  Order.find({ oMember })
    .exec()
    .then(orders => res.status(200).json({ orders }))
    .catch(err => res.status(500).json({ err }));
});

//------------------------------------------- Add new order
router.post("/", async (req, res) => {
  const { oMember, oItems, oDate } = req.body;
  const newOrder = new Order({ oMember, oItems, oDate });
  console.log({ newOrder });
  const resNewOrder = await newOrder.save();
  if (resNewOrder) return res.status(200).json({ MSG: "Order Submit" });
  else return res.status(500).json({ MSG: "Cannot creat order" });
});

module.exports = router;
