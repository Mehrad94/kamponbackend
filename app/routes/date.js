const express = require("express");
const router = express.Router();
var moment = require("jalali-moment");

router.get("/", async (req, res, next) => {
  //get current date
  const persianDate = moment()
    .locale("fa")
    .format("YYYY/M/D");

  res.status(200).json({ date: persianDate });
});

module.exports = router;
