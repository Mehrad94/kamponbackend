const express = require("express");
const router = express.Router();
const Category = require("app/models/category");
const mongoose = require("mongoose");

//------------------------------------------- Get all categories
router.get("/", async (req, res) => {
  try {
    const resGetAllCats = await Category.find({ activeCount: { $gt: 0 } });
    res.send(resGetAllCats);
  } catch (e) {
    console.log({ e });
  }
});

// ========================= get All SubCategories =======================
router.get("/:catId", async (req, res) => {
  try {
    console.log(" ================= Member Get Subcategories ==============");
    console.log({ params: req.params });
    console.log({ body: req.body });
    console.log({ headers: req.headers });
    const resGetAllCats = await Category.findOne({
      _id: mongoose.Types.ObjectId(req.params.catId)
    });
    res.send(resGetAllCats.subCategories);
  } catch (e) {
    console.log({ e });
  }
});

module.exports = router;
