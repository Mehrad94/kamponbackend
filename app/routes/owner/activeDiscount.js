const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkOwnerToken");

router.post("/", checkToken, async (req, res) => {
    
});

module.exports = router;
