const express = require("express");
const router = express.Router();
const oLogin = require("app/models/logins/oLogin");
const jwt = require("jsonwebtoken");
const Owner = require("app/models/owner");
const securityConstants = require("app/constants/Security");

router.post("/", async (req, res) => {
  //check if oPhoneNumber or oCode are not empty
  if (!req.body.oPhoneNumber || !req.body.oCode)
    return res.status(500).json({ ERROR: 1011 });

  //object destructuring
  const { oPhoneNumber, oCode } = req.body;

  //log
  console.log({ oPhoneNumber, oCode });

  //check if this phone number is registered
  const resGetOwner = await Owner.findOne({ oPhoneNumber });
  if (!resGetOwner) return res.status(500).json({ ERROR: 1082 });

  console.log({ resGetOwner });

  //Get Owner Info from Owner Model to Save them in token
  const resGetOLogin = await oLogin.findOne({ oPhoneNumber, oToken: oCode });
  if (!resGetOLogin) return res.status(500).json({ ERROR: 1014 });

  //Generate Token
  const token = jwt.sign(
    {
      oPhoneNumber,
      _id: resGetOwner._id,
      isAdmin: false,
      isOwner: true,
      isMember: false
    },
    securityConstants.tokensKey,
    { expiresIn: "9000h" }
  );

  console.log({ token });
  //Send the token
  res.status(200).json({ token });
});

module.exports = router;

//1013 : badRequest
