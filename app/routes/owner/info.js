const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const Owner = require("app/models/owner");
const ownersTransaction = require("app/models/ownersTransaction");
const Discount = require("app/models/discount");
const Order = require("app/models/order");

router.get("/", checkOwnerStatus, async (req, res) => {
  //introduction
  console.log("===================== Owner Get Info =====================");

  //get owner id from his token
  const { _id: ownerId } = req.tokenInfo;

  //log
  console.log({ ownerId });

  //initialize owners balance
  let balance = 0;

  //get all owners transActions
  const resGetTransactions = await ownersTransaction.find({
    otOwner: ownerId
  });

  //log
  console.log({ resGetTransactions });

  //map trough transactions and calculate total balance
  resGetTransactions.map(transaction => {
    balance += transaction.otAmount;
  });

  //log
  console.log({ balance });

  //get owner profile
  const ownerInfo = await Owner.findOne({
    _id: ownerId
  });

  //log
  console.log({ ownerInfo });

  //get Active discounts
  const activeDiscounts = await Discount.find({
    isActive: true,
    ownerId
  });

  //log
  console.log({ activeDiscounts });

  //get Deactived discounts
  const deactiveDiscounts = await Discount.find({
    isActive: false,
    ownerId
  });

  //log
  console.log({ deactiveDiscounts });

  //get used orders
  const usedOrders = await Order.find({
    isUsed: true,
    oOwner: ownerId
  });

  //log
  console.log({ usedOrders });

  //get not used
  const notUsedOrders = await Order.find({
    isUsed: false,
    oOwner: ownerId
  });

  //log
  console.log({ notUsedOrders });

  //generate response object
  const response = {
    balance,
    ownerInfo,
    activeDiscounts: activeDiscounts.length,
    deactiveDiscounts: deactiveDiscounts.length,
    usedOrders: usedOrders.length,
    notUsedOrders: notUsedOrders.length
  };

  //log
  console.log({ response });

  //send response to the client
  res.status(200).json(response);
});

module.exports = router;
