const jwt = require("jsonwebtoken");
const Security = require("app/constants/Security");
const express = require("express");
const router = express.Router();
const Owner = require("app/models/owner");
const mongoose = require("mongoose");
router.get("/", async (req, res, next) => {
  console.log("============= Owner Check Token Route ================");
  console.log({ Header: req.headers });
  try {
    const token = req.headers.authorization.split(" ")[1];
    const tokenInfo = jwt.verify(token, Security.tokensKey);
    console.log({ tokenInfo });
    const { _id } = tokenInfo;
    const resGetOwner = await Owner.findOne({
      _id
    });
    console.log({ resGetOwner });
    if (
      !tokenInfo.isAdmin &&
      tokenInfo.isOwner &&
      !tokenInfo.isMember &&
      resGetOwner.oIsActive
    )
      res.status(200).send("OK");
    else res.status(400).json({ ERROR: 1014 });
  } catch (e) {
    console.log({ e });
    res.status(400).json({ ERROR: 1014 });
  }
});

module.exports = router;
