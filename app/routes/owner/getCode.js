const express = require("express");
const router = express.Router();
const oLogin = require("app/models/logins/oLogin");
const Owner = require("app/models/owner");
const Security = require("app/constants/Security");
const { sendLoginSMS } = require("app/constants/sendSMS");

router.post("/", async (req, res) => {
  console.log("==================== Owner Get Code ====================");

  //get nesessary tings from req.body
  const { oPhoneNumber } = req.body;

  //log
  console.log({ oPhoneNumber });

  //find owner from database
  const resGetOwner = await Owner.findOne({ oPhoneNumber });

  //log
  console.log({ resGetOwner });

  //send an error if this owner doesnt exist
  if (!resGetOwner) return res.status(500).json({ ERROR: 1082 });

  //check if owner is active
  if (!resGetOwner.oIsActive) return res.status(500).json({ ERROR: 1083 });

  //generate a 4 digit
  const oCode = Security.generateRandomNumber(4);

  //upsert oCode in oLogin db
  const resAddNewOwnerLogin = await oLogin.findOneAndUpdate(
    {
      oPhoneNumber
    },
    { $set: { oToken: oCode, oPhoneNumber } },
    { upsert: true, new: true }
  );

  //log
  console.log({ resAddNewOwnerLogin });

  //if upsertion done
  if (resAddNewOwnerLogin) {
    //send sms
    await sendLoginSMS(oPhoneNumber, oCode, res);
  }
});

module.exports = router;
