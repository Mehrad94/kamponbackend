const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const Order = require("app/models/order");
const mongoose = require("mongoose");

router.post("/", checkOwnerStatus, async (req, res) => {
  console.log("============= Check Track Code  ================");

  //Check if order string is valid
  try {
    const { trackCode } = req.body;
    const { _id: ownerId } = req.tokenInfo;

    //find order
    console.log({ trackCode, ownerId });

    //get order from db
    const resGetOrder = await Order.findOne({
      oOwner: mongoose.Types.ObjectId(ownerId),
      trackCode
    }).populate("oMember disId");

    //log
    console.log({ resGetOrder });

    //if this order doesnt exist send an error
    if (!resGetOrder) return res.status(500).json({ ERROR: 1093 });

    //make the out put order
    let order = {
      disImage: resGetOrder.disId.disImages[0],
      disNewPrice: resGetOrder.disId.disNewPrice,
      disRealPrice: resGetOrder.disId.disRealPrice,
      disPercent: resGetOrder.disId.disPercent,
      disTitle: resGetOrder.disId.disTitle,
      mName: resGetOrder.oMember.mName,
      mAvatar: resGetOrder.oMember.mAvatar,
      isUsed: resGetOrder.isUsed
    };

    //log
    console.log({ order });

    //send the final result
    res.status(200).json(order);
  } catch (e) {
    res.status(500).send({ ERROR: 1093 });
  }
});
module.exports = router;

//to do: admin check token is defrent from owner and member
