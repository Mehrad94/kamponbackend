const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const oTransaction = require("app/models/ownersTransaction");

router.get("/", checkOwnerStatus, async (req, res) => {
  console.log("================== Owner get Transactions ==================");

  //get all transactions from database
  const resGetTransActions = await oTransaction.find({
    otOwner: req.tokenInfo._id
  });

  //log
  console.log({ resGetTransActions });

  //if there is no transaction send Error 9010
  if (!resGetTransActions.length) return res.status(500).json({ ERROR: 9010 });

  //send result to the client
  res.status(200).json(resGetTransActions);
});

module.exports = router;
