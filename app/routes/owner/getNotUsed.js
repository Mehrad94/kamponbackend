const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const Order = require("app/models/order");

router.get("/", checkOwnerStatus, async (req, res) => {
  console.log(
    "================== Owner get Not Used Discounts =================="
  );

  console.log({ tokenInfo: req.tokenInfo });

  try {
    const notUsedOrders = await Order.find({
      isUsed: false,
      oOwner: req.tokenInfo._id
    });
    console.log({ notUsedOrders });

    res.status(200).json(notUsedOrders);
  } catch (e) {
    console.log({ e });
  }
});

module.exports = router;
