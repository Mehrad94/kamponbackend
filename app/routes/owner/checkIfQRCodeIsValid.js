const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const jwt = require("jsonwebtoken");
const Owner = require("app/models/owner");
const Order = require("app/models/order");
const security = require("app/constants/Security");

router.post("/", checkOwnerStatus, async (req, res) => {
  console.log("============= Check Qr Code  ================");

  //Check if order string is valid
  try {
    const { orderString } = req.body;
    const { _id: ownerId } = req.tokenInfo;

    //extract id's from order string
    const oMember = orderString.substr(0, 24);
    const oOwner = orderString.substr(24, 24);
    const disId = orderString.substr(48, 24);
    const number = orderString.substr(72);

    //Authenticate owner
    if (ownerId != oOwner) return res.status(500).send({ ERROR: 1093 });

    //find order
    console.log({ orderString, oMember, oOwner, disId });

    //get order from db
    const resGetOrder = await Order.findOne({
      oMember,
      oOwner,
      disId,
      number
    }).populate("oMember disId");

    //if this order doesnt exist send an error
    if (!resGetOrder) return res.status(500).json({ ERROR: 1093 });

    //log
    console.log({ resGetOrder });

    //make the out put order
    let order = {
      disImage: resGetOrder.disId.disImages[0],
      disNewPrice: resGetOrder.disId.disNewPrice,
      disRealPrice: resGetOrder.disId.disRealPrice,
      disPercent: resGetOrder.disId.disPercent,
      disTitle: resGetOrder.disId.disTitle,
      mName: resGetOrder.oMember.mName,
      mAvatar: resGetOrder.oMember.mAvatar
    };

    //log
    console.log({ order });

    //send the final result
    res.status(200).json(order);
  } catch (e) {
    res.status(500).send({ ERROR: 1093 });
  }
});
module.exports = router;

//to do: admin check token is defrent from owner and member
