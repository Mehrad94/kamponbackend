const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const Discount = require("app/models/discount");

router.get("/", checkOwnerStatus, async (req, res) => {
  const activeDiscounts = await Discount.find({
    isActive: true,
    ownerId: req.tokenInfo._id
  });
  res.status(200).json(activeDiscounts);
});

module.exports = router;
