const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const Discount = require("app/models/discount");

router.get("/", checkOwnerStatus, async (req, res) => {
  const deactiveDiscounts = await Discount.find({
    isActive: false,
    ownerId: req.tokenInfo._id
  });
  res.status(200).json(deactiveDiscounts);
});

module.exports = router;
