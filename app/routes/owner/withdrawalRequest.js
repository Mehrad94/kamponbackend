const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const WithdrawalRequest = require("app/models/withdrawalRequest");
const callPython = require("./../../constants/callPython");
const getTime = require("../../constants/getTime");
const { sendWidthdrawalSMS } = require("app/constants/sendSMS");
const Owner = require("app/models/owner");
const mongoose = require("mongoose");

router.get("/", checkOwnerStatus, async (req, res, next) => {
  console.log("============== Owner Withdrawal Request  ==============");

  try {
    //get ownerId from token
    const { _id: ownerId } = req.tokenInfo;

    //log
    console.log({ ownerId });

    //ask python for this owners balance and do the rest inside the callback
    callPython("preventRepetetiveAskForWithdrawal.py", ownerId, result => {
      //delete \r and \n from the end of the result
      result = result.substr(0, 1);

      //log
      console.log({ isValid: result });

      //"0" means this request is repetetive and should be prevented
      if (result === "0") {
        //log
        console.log({ ERROR: 1055 });

        //send the result to the client
        return res.status(500).json({ ERROR: 1055 });
      }

      //ask python for this owners balance
      callPython("ownerBalanceCalculation.py", ownerId, async result => {
        //delete \r and \n from the end of the result
        let balance = result
          .replace("\r\n", "")
          .replace("\n", "")
          .replace("\r", "");

        //log
        console.log({ balance });

        //if balance is zero return an error
        if (balance == "0") {
          //log
          console.log({ ERROR: 1056 });

          //send the result to the client
          return res.status(500).json({ ERROR: 1056 });
        }

        //add a new request to the database
        new WithdrawalRequest({
          owner: ownerId,
          amount: Number(balance),
          time: getTime()
        }).save();

        //get Owner Info
        const resGetOwner = await Owner.findOne({
          _id: mongoose.Types.ObjectId(_id)
        });

        //send notification sms to admin
        await sendWidthdrawalSMS(
          resGetOwner.oPhoneNumber,
          resGetOwner.oName,
          balance,
          res
        );
      });
    });
  } catch (error) {
    console.log({ error });
    res.status(500).json({ ERROR: 5010 });
  }
});

module.exports = router;
