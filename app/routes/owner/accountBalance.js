const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
//const transaction = require("app/models/transAction");

router.get("/", checkOwnerStatus, async (req, res) => {
  //initialize balance
  let balance = 0;

  //get all transactions of this owner
  const resGetAllTransaction = await transaction.find({
    owner: req.tokenInfo._id
  });

  //map in all retrieved transactions
  resGetAllTransaction.map(transaction => {
    balance += transaction.amount;
  });

  res.status(200).json({ balance });
});

module.exports = router;
