const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkOwnerStatus");
const Order = require("app/models/order");
const oTransaction = require("app/models/ownersTransaction");
const getTime = require("app/constants/getTime");

router.post("/", checkToken, async (req, res) => {
  console.log("================== Owner IsUsed ==================");
  const { orderString } = req.body;
  const { _id: ownerId } = req.tokenInfo;
  console.log({ orderString });

  try {
    //extract order string information
    const oMember = orderString.substr(0, 24);
    const oOwner = orderString.substr(24, 24);
    const disId = orderString.substr(48, 24);
    const number = orderString.substr(72);

    //log
    console.log({ oMember, oOwner, disId, number });

    //Authenticate owner
    if (ownerId != oOwner) return res.status(500).send({ ERROR: 1093 });

    //get order from database
    const resGetAndUpdateOrder = await Order.findOneAndUpdate(
      { oMember, oOwner, disId, number },
      { isUsed: true, time: getTime() }
    );

    //log
    console.log({ resGetAndUpdateOrder });

    if (resGetAndUpdateOrder) {
      //check if this order was used
      if (resGetAndUpdateOrder.isUsed)
        return res.status(400).json({ ERROR: 1094 });

      //get total price
      const { oOwner, oNewPrice, oTitle, oAuthority } = resGetAndUpdateOrder;

      //make new transaction
      const transAction = {
        otTime: getTime(),
        otOwner: oOwner,
        otAmount: oNewPrice,
        otDescription: " خرید بابت " + oTitle,
        otAuthority: oAuthority
      };

      //add transaction todatabase
      const resAddNewTransactionToDb = await new oTransaction(
        transAction
      ).save();

      res.status(200).json({ CODE: 2098 });
    } else {
      res.status(400).json({ CODE: 2012 });
    }
  } catch (e) {
    res.status(400).json({ e });
  }
});

router.post("/trackCode", checkToken, async (req, res) => {
  console.log("================== Owner IsUsed ==================");
  const { trackCode } = req.body;
  const { _id: oOwner } = req.tokenInfo;
  console.log({ trackCode, oOwner });

  try {
    //get order from database and convert is used to true
    const resGetAndUpdateOrder = await Order.findOneAndUpdate(
      { trackCode, oOwner },
      { isUsed: true, time: getTime() }
    );

    //log
    console.log({ resGetAndUpdateOrder });

    if (resGetAndUpdateOrder) {
      //check if this order was used
      if (resGetAndUpdateOrder.isUsed)
        return res.status(400).json({ ERROR: 1094 });

      //get total price
      const { oOwner, oNewPrice, oTitle, oAuthority } = resGetAndUpdateOrder;

      //make new transaction
      const transAction = {
        otTime: getTime(),
        otOwner: oOwner,
        otAmount: oNewPrice,
        otDescription: " خرید بابت " + oTitle,
        otAuthority: oAuthority.replace("0", "")
      };

      //add transaction todatabase
      const resAddNewTransactionToDb = await new oTransaction(
        transAction
      ).save();

      console.log({ CODE: 2098 });
      res.status(200).json({ CODE: 2098 });
    } else {
      console.log({ CODE: 2012 });
      res.status(400).json({ CODE: 2012 });
    }
  } catch (e) {
    res.status(400).json({ e });
  }
});
module.exports = router;

//CODE: 2098 ==> isUsedDone
//CODE: 1012  ==> isUsedFailed
