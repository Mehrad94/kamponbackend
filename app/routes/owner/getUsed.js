const express = require("express");
const router = express.Router();
const checkOwnerStatus = require("app/middleware/checkOwnerStatus");
const Order = require("app/models/order");

router.get("/", checkOwnerStatus, async (req, res) => {
  console.log("================== Owner get Used Discounts ==================");
  const usedOrders = await Order.find({
    isUsed: true,
    oOwner: req.tokenInfo._id
  });
  res.status(200).json(usedOrders);
});

module.exports = router;
