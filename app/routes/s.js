const express = require("express");
const router = express.Router();
const Slide = require("app/models/slide");
const Discount = require("app/models/discount");

//------------------------------------------- Get slide by type
router.get("/:sType", async (req, res) => {
  console.log("============ Member Get " + req.params.sType + " Sliders");
  const sType = req.params.sType;
  const slidesByType = await Slide.find({ sType });
  console.log({ slidesByType });
  let slides = [];
  for (let i = 0; i < slidesByType.length; i++) {
    const getDiscountById = await Discount.findOne({
      _id: slidesByType[i].sDiscountId
    });

    if (getDiscountById) {
      const newSlide = {
        sliderId: slidesByType[i]._id,
        sImage: slidesByType[i].sImage,
        sTitle: getDiscountById.disTitle,
        sPercent: getDiscountById.disPercent,
        sDiscountId: getDiscountById._id,
        sCategory: getDiscountById.disCategory
      };
      slides.push(newSlide);
    }
  }
  res.status(201).json(slides);
});

//------------------------------------------- Add new category
router.post("/", (req, res) => {
  const { sType, sImage, sDiscountId } = req.body;
  const slide = new Slide({ sType, sImage, sDiscountId });
  slide
    .save()
    .then(() => {
      res.status(201).json({ message: "Slide Created" });
    })
    .catch(err => res.status(500).json({ err }));
});

//------------------------------------------- Delete discount by id
router.delete("/:slideId", (req, res) => {
  const _id = req.params.slideId;
  Slide.remove({ _id })
    .exec()
    .then(() => {
      res.status(200).json({ message: "Slide Deleted" });
    })
    .catch(err => res.status(s500).json({ err }));
});

module.exports = router;
