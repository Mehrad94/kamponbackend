const express = require("express");
const imgModel = require("app/models/image");
const router = express.Router();
const multer = require("multer");
const checkToken = require("app/middleware/checkMemberToken");
const { SERVER } = require("app/values/strings");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    console.log("destination");
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    console.log("filename");
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg") {
    cb(null, true);
  } else {
    cb("Error: Just .jpg and .jpeg files are allowed", false);
  }
};

const upload = multer({
  storage,
  limits: { fileSize: 1024 * 1024 * 6 },
  fileFilter
});

router.post("/", upload.single("image"), async (req, res) => {
  console.log(" ================ Member Upload Image ================");
  //log
  console.log({ file: req.file });

  //send the url to the client
  res.send({
    url: SERVER + req.file.path
  });
});

module.exports = router;
