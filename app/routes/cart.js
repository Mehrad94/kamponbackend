const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const Owner = require("app/models/owner");
const Cart = require("app/models/cart");
const Discount = require("app/models/discount");
const tokenCheck = require("app/middleware/checkMemberToken");
const mongoose = require("mongoose");

router.get("/", tokenCheck, async (req, res, next) => {
  console.log("========================= Get Cart =================");
  const _id = req.tokenInfo._id;
  console.log({ _id });
  try {
    const resGetCart = await Cart.find({ cartBuyer: _id }).populate(
      "cartDiscount"
    );
    res.status(200).json({ resGetCart });
  } catch (error) {
    res.status(400).json({ error: "Somthing Went Wrong" });
  }
});

router.put("/:discountId/:count", tokenCheck, async (req, res) => {
  console.log("========================= Member Update Cart =================");
  const buyerId = req.tokenInfo._id;
  const { discountId, count } = req.params;
  console.log({ discountId, count });
  try {
    //get discount
    const resGetOwner = await Discount.findOne({
      _id: mongoose.Types.ObjectId(discountId)
    });

    //check if this discount exists
    if (!resGetOwner) return res.status(500).json({ CODE: 1011 });

    const resUpdateCount = await Cart.findOneAndUpdate(
      {
        cartBuyer: buyerId,
        cartDiscount: discountId,
        discountOwner: resGetOwner.ownerId
      },
      { $inc: { count: Number(count) } },
      { new: true, upsert: true }
    );

    //log
    console.log({ resUpdateCount });

    //delete cartItem if count is 0
    if (resUpdateCount.count <= 0) {
      const resDeleteCartItem = await Cart.deleteOne({
        cartBuyer: buyerId,
        cartDiscount: discountId
      });

      console.log({ resDeleteCartItem });
    }

    //send the result to the client
    if (resUpdateCount) {
      res.status(200).json({ CODE: 3064 });
    } else {
      res.status(500).json({ ERROR: 1064 });
    }
  } catch (e) {
    res.status(500).json({ ERROR: 1027 });
  }
});

router.delete("/:discountId", tokenCheck, async (req, res) => {
  console.log("========================= Member Delete Cart =================");
  const buyerId = req.tokenInfo._id;
  const { discountId } = req.params;
  console.log({ discountId });
  try {
    const resDeleteCart = await Cart.deleteOne({
      cartBuyer: buyerId,
      cartDiscount: discountId
    });

    //log
    console.log({ resDeleteCart });

    //send the result to the client
    if (resDeleteCart.deletedCount > 0) {
      res.status(200).json({ CODE: 3064 });
    } else {
      res.status(500).json({ ERROR: 1064 });
    }
  } catch (e) {
    res.status(500).json({ ERROR: 1027 });
  }
});

module.exports = router;
