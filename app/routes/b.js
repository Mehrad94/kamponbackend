const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const checkAuth = require("app/middleware/checkMemberToken");

// ----------------------------------------- Add to cart
router.post("/", checkAuth, async (req, res) => {
  const { discountId, count, memberId } = req.body;
  console.log({ discountId, count, memberId });
  const resFindMember = await Member.findOne({ _id: memberId });
  let flagCheckCart = false;

  //check there is a user with this id or not
  if (resFindMember == null)
    return res.status(200).json({ msg: "Cannot find member" });

  //check there is a discount in cart or not
  const mCart = resFindMember.mCart;
  await mCart.map(cart => {
    if (discountId === cart.discountId) {
      cart.count = count;
      flagCheckCart = true;
    }
  });
  if (!flagCheckCart) {
    const newItem = { discountId, count };
    mCart.push(newItem);
  }
  const resUpdateCart = await Member.findOneAndUpdate(
    { _id: memberId },
    { $set: { mCart } },
    { new: true }
  );
  if (resUpdateCart) return res.status(200).json({ msg: "Cart updated" });
  else return res.status(500).json({ msg: "Cannot update cart" });
});

module.exports = router;
