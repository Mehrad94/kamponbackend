const express = require('express');
const router = express.Router();
const Owner = require('../models/owner');

//------------------------------------------- Get all discount
router.get('/', (req, res) => {
    Owner.find()
        .exec()
        .then(owners => res.status(200).json({owners}))
        .catch(err => res.status(500).json({err}));
});

//------------------------------------------- Add new discount
router.post('/', (req, res) => {
    const {oName, oUsername, oAddress, oPhoneNumber, oDistrict, oCategory, oAvatar, oLocation} = req.body;
    const owner = new Owner({oName, oUsername, oAddress, oPhoneNumber, oDistrict, oCategory, oAvatar, oLocation});
    owner.save()
        .then((result) => {
            res.status(201).json({message: 'Owner Created', result});
        })
        .catch(err => res.status(500).json({err}));
});

//------------------------------------------- Delete category by id
router.delete('/:ownerId', (req, res) => {
    const id = req.params.ownerId;
    Owner.deleteOne({_id: id})
        .exec()
        .then(() => {
            res.status(200).json({message: 'Owner Deleted'})
        })
        .catch(err => res.status(500).json({err}));
});

module.exports = router;