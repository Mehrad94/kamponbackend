// import { SUCCESS_ADD_BOOKMARK } from "app/values";
const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const Discount = require("app/models/discount");
const mongoose = require("mongoose");
const strings = require("app/values/strings");
const tokenCheck = require("app/middleware/checkMemberToken");

router.get("/", tokenCheck, async (req, res) => {
  const resFindBookmarks = await Member.findOne({
    mPhoneNumber: req.tokenInfo.mPhoneNumber
  }).populate("bookmarks");
  res.status(200).json({ resFindBookmarks });
});

router.get("/check/:discountId", tokenCheck, async (req, res, next) => {
  console.log("=================== Member Check Bookmark ================");
  try {
    const userId = req.tokenInfo._id;
    const { discountId } = req.params;

    const resGetMember = await Member.findOne({
      _id: mongoose.Types.ObjectId(userId)
    });

    console.log({ resGetMember });

    if (resGetMember.bookmarks.indexOf(discountId) > -1)
      return res.status(200).json({ CODE: 2097 });
    else return res.status(500).json({ ERROR: 1097 });
  } catch (e) {
    console.log({ e });
    return res.status(500).json({ ERROR: 1097 });
  }
});

router.post("/", tokenCheck, async (req, res) => {
  console.log(" ==================== Member Toggle Bookmark ================");
  console.log({ discountId: req.body.discountId });
  console.log({ mPhoneNumber: req.tokenInfo.mPhoneNumber });

  const { discountId } = req.body;

  const resFindDiscount = await Discount.findOne({
    _id: mongoose.Types.ObjectId(discountId)
  });

  if (!resFindDiscount)
    return res.status(500).json({ CODE: strings.BAD_REQUEST });

  const { bookmarks } = await Member.findOne({
    mPhoneNumber: req.tokenInfo.mPhoneNumber
  });

  if (bookmarks.indexOf(discountId) > -1) {
    const resDeleteBookmark = await Member.updateMany(
      { mPhoneNumber: req.tokenInfo.mPhoneNumber },
      { $pullAll: { bookmarks: [discountId] } }
    );

    console.log({ CODE: strings.SUCCESS_DELETE });
    res.status(200).json({ CODE: strings.SUCCESS_DELETE });
  } else {
    const resUpdateBookmark = await Member.findOneAndUpdate(
      { mPhoneNumber: req.tokenInfo.mPhoneNumber },
      { $addToSet: { bookmarks: discountId } },
      { new: true }
    );
    console.log({ CODE: strings.SUCCESS_ADD });
    res.status(200).json({ CODE: strings.SUCCESS_ADD });
  }
});

module.exports = router;
