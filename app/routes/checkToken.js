const jwt = require("jsonwebtoken");
const securityConstants = require("app/constants/Security");
const express = require("express");
const router = express.Router();
const Member = require("app/models/member");

router.get("/", async (req, res, next) => {
  try {
    console.log("============ Member Check Token ==============");
    console.log({ token: req.headers.authorization });
    const token = req.headers.authorization.split(" ")[1];

    const { mPhoneNumber } = jwt.verify(token, securityConstants.tokensKey);

    const resGetMember = await Member.findOne({ mPhoneNumber });
    console.log({ token, mPhoneNumber });

    if (!resGetMember) {
      console.log({ CODE: 1012 });
      return res.status(500).json({ CODE: 1012 });
    }
    console.log({ CODE: 2022 });
    res.status(200).json({ CODE: 2022 });
  } catch (e) {
    res.status(500).json({ CODE: 1012 });
  }
});

module.exports = router;
