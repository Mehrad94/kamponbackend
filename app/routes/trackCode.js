const express = require("express");
const router = express.Router();
const Security = require("app/constants/Security");
const TrackCode = require("app/models/trackCode");

router.get("/", async (req, res, next) => {
  // const code = Security.IDGenerator();
  // const resAddTrackCode = await new TrackCode({ code }).save();
  // console.log({ code });
  // res.status(200).json({ code });

  res.redirect("https://www.kampon.ir");
});

module.exports = router;
