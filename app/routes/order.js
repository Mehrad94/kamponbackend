const express = require("express");
const router = express.Router();
const Cart = require("app/models/cart");
const Order = require("app/models/order");
const Discount = require("app/models/discount");
const mTransaction = require("app/models/memberTransaction");
const request = require("request-promise");
const getTime = require("app/constants/getTime");
const Security = require("app/constants/Security");

time = getTime();

router.get("/", async (req, res) => {
  console.log(
    "============================== Bank Callback =============================="
  );
  //get Status and Authority
  const { Status, Authority } = req.query;

  //Check if status is OK
  if (Status !== "OK") {
    return res.redirect(
      "https://www.kampon.ir/payment/0/4fs6d4fs65df4s6df4s6df5"
    );
  }

  //get cartDiscount, discountOwner from Authority
  const resGetCart = await Cart.find({
    Authority
  }).populate("cartDiscount discountOwner cartBuyer");

  console.log({ resGetCart });
  //return if a cart with this Authority doesnt exist
  if (resGetCart.lenght == 0) return res.status(500).json({ CODE: 1021 });

  //calculate price
  let totalPrice = 0;
  resGetCart.map(cartItem => {
    totalPrice += cartItem.count * cartItem.cartDiscount.disNewPrice;
  });

  console.log({ totalPrice });
  // get ready the params that should be sent to zarinpal
  let params = {
    MerchantID: "3510518c-ab28-11e9-a354-000c29344814",
    Amount: totalPrice.toString(),
    Authority
  };

  console.log({ params });
  // get ready the connection options for connecting to zarinpal
  let options = {
    method: "POST",
    uri: "https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json",
    headers: {
      "cache-control": "no-cache",
      "content-type": "aplication/json"
    },
    body: params,
    json: true
  };

  console.log({ options });
  // connect to the zarinpal to check the Authority of payment
  request(options)
    .then(async data => {
      if (data.Status !== 100) {
        //redirect the user to the failed page
        return res.redirect(
          "https://www.kampon.ir/payment/0/4fs6d4fs65df4s6df4s6df5"
        );
      }

      //get the discount from cart
      const resGetYourCartItems = await Cart.find({
        Authority
      }).populate("cartBuyer cartDiscount discountOwner");

      //get total count for for loop
      let totalCount = 0;
      resGetYourCartItems.map(cart => {
        totalCount += cart.count;
      });

      //log
      console.log({ totalCount });

      //delete the discount from cart
      const resFindAndDeleteCart = await Cart.deleteMany({
        Authority
      });

      console.log({ resGetYourCartItems });

      for (let j = 0; j < resGetYourCartItems.length; j++) {
        const cartItem = resGetYourCartItems[j];
        console.log({ cartItem });
        for (let i = 0; i < cartItem.count; i++) {
          //make a new order
          const newOrder = {
            number: i,
            time,
            disId: cartItem.cartDiscount._id,
            oMember: cartItem.cartBuyer._id,
            oCount: cartItem.count,
            oTitle: cartItem.cartDiscount.disTitle,
            oDescription: cartItem.cartDiscount.disDescription,
            oImages: cartItem.cartDiscount.disImages,
            oPercent: cartItem.cartDiscount.disPercent,
            oRealPrice: cartItem.cartDiscount.disRealPrice,
            oNewPrice: cartItem.cartDiscount.disNewPrice,
            oCategory: cartItem.cartDiscount.disCategory,
            oCategoryPersian: cartItem.cartDiscount.disCategoryPersian,
            oOwner: cartItem.discountOwner._id,
            oDistrict: cartItem.cartDiscount.disDistrict,
            oFeatures: cartItem.cartDiscount.disFeatures,
            oTermsOfUse: cartItem.cartDiscount.disTermsOfUse,
            oAuthority: cartItem.Authority,
            trackCode: Security.IDGenerator()
          };

          //add order to database
          const resAddNewOrderToDb = await new Order(newOrder).save();
          console.log({ resAddNewOrderToDb });

          //add bougth count of this discount
          const resAddDiscountBougthCount = await Discount.findOneAndUpdate(
            { _id: cartItem.cartDiscount._id },
            { $inc: { boughtCount: 1 } },
            { new: true }
          );
          console.log({ resAddDiscountBougthCount });

          //make new transaction
          const transAction = {
            mtTime: time,
            mtOwner: cartItem.discountOwner._id,
            mtAmount: totalPrice,
            mtDescription: cartItem.cartDiscount.disTitle + "خرید بابت",
            mtAuthority: Authority
          };

          //add transaction todatabase
          const resAddNewTransactionToDb = await new mTransaction(
            transAction
          ).save();
          console.log({ resAddNewTransactionToDb });
        }
      }

      //redirect the user to the success page
      return res.redirect(
        "https://www.kampon.ir/payment/1/4fs6d4fs65df4s6df4s6df5"
      );
    })

    .catch(error => {
      console.log({ error });
      return res.redirect(
        "https://www.kampon.ir/payment/0/4fs6d4fs65df4s6df4s6df5"
      );
    });
});

module.exports = router;

// CODE: 1025 ==> payment Failed
