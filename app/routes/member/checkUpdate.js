const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkMemberToken");
const Member = require("app/models/member");
const LastVersion = require("app/models/lastVersion");
const Settings = require("app/models/settings");
const mongoose = require("mongoose");

router.get("/:version", async (req, res) => {
  console.log("=================== Member Check Update ================");
  try {
    const { version } = req.params;

    const resGetVersion = await LastVersion.find();

    if (version === resGetVersion[0].memberAppVersion) {
      res.status(200).json({ CODE: 2045 });
    } else {
      const resGetSettings = await Settings.findOne();
      console.log({ resGetSettings });
      res
        .status(200)
        .json({ ERROR: 1045, LINK: resGetSettings.sLinks.memberApplication });
    }
  } catch (e) {
    console.log({ e });
    return res.status(500).json({ ERROR: 1097 });
  }
});

router.post("/", async (req, res, next) => {
  const resAddVersion = await new LastVersion({
    memberAppVersion: "1.0.0",
    sellerAppVersion: "1.0.0"
  }).save();
  console.log({ resAddVersion });
  res.status(200).json({ msg: "msg" });
});

module.exports = router;

//CODE: 2057 ==> this discount exists in the cart
//CODE: 1032 ==> doesnt exist
