const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkMemberToken");
const Member = require("app/models/member");
const mongoose = require("mongoose");

router.get("/:discountId", checkToken, async (req, res) => {
  console.log("=================== Member Check Bookmark ================");
  try {
    const userId = req.tokenInfo._id;
    const { discountId } = req.params;

    const { bookmarks } = await Member.findOne({
      _id: mongoose.Types.ObjectId(userId)
    });

    if (bookmarks.indexOf(discountId) > -1)
      return res.status(200).json({ CODE: 2097 });
    else return res.status(500).json({ ERROR: 1097 });
  } catch (e) {
    console.log({ e });
    return res.status(500).json({ ERROR: 1097 });
  }
});

module.exports = router;

//CODE: 2057 ==> this discount exists in the cart
//CODE: 1032 ==> doesnt exist
