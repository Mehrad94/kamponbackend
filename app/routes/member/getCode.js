const express = require("express");
const router = express.Router();
const mLogin = require("app/models/logins/mLogin");
const Member = require("app/models/member");
const Security = require("app/constants/Security");
const { sendLoginSMS } = require("app/constants/sendSMS");
const strings = require("app/values/strings");

router.post("/", async (req, res) => {
  console.log("==================== Member Get Code ====================");

  try {
    //get nesessary tings from req.body
    const { mPhoneNumber } = req.body;

    if (!mPhoneNumber) return res.status(500).json({ CODE: 1064 });

    //log
    console.log({ mPhoneNumber });

    //get Member Info
    const resGetMember = await Member.findOne({ mPhoneNumber });

    //check if he is not blocked
    if (resGetMember && !resGetMember.mIsActive) {
      return res.status(500).json({ CODE: 1010 });
    }

    //initialize isNewMember
    let isNewMember = true;

    if (resGetMember) isNewMember = resGetMember.isNewMember;

    //generate a 4 digit
    const mCode = Security.generateRandomNumber(4);

    //upsert mCode in mLogin db
    const resAddNewMemberLogin = await mLogin.findOneAndUpdate(
      {
        mPhoneNumber
      },
      { $set: { mCode, mPhoneNumber } },
      { upsert: true, new: true }
    );

    //log
    console.log({ resAddNewMemberLogin });

    //if upsertion done
    if (resAddNewMemberLogin) {
      //send sms, and send the response in it
      await sendLoginSMS(mPhoneNumber, mCode, res, isNewMember);
    }
  } catch (e) {
    console.log({ e });
    res.status(500).json({ CODE: strings.BAD_REQUEST });
  }
});

module.exports = router;
