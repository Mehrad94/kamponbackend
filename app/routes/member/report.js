const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkMemberToken");
const mongoose = require("mongoose");
const Discount = require("app/models/discount");
const Report = require("app/models/report");
const getTime = require("app/constants/getTime");

router.post("/", checkToken, async (req, res) => {
  console.log("=========== Member Report ==========");

  try {
    const { disId, subjects, extraExplaination } = req.body;
    const { _id: memberId } = req.tokenInfo;

    //check if requierd fields are filled
    if (!disId || !subjects.length || !memberId)
      return res.status(500).json({ ERROR: 5010 });

    //get discount
    const resGetDiscount = await Discount.findOne({
      _id: mongoose.Types.ObjectId(disId)
    });

    //log
    console.log({ resGetDiscount });

    //add report
    const resAddReport = await new Report({
      disId,
      subjects,
      extraExplaination,
      memberId,
      time: getTime()
    }).save();

    console.log({ resAddReport });

    res.status(200).json({ CODE: 2026 });
  } catch (error) {
    console.log({ error });
    res.status(500).json({ ERROR: 1010 });
  }
});

module.exports = router;
