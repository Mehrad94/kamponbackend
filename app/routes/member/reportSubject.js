const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const ReportSubject = require("app/models/reportSubject");

router.get("/", async (req, res) => {
  console.log("\n\n=============== Member Get Reports Items ===============");

  //find all reports from database
  let resGetReports = await ReportSubject.find();

  //final array
  finalArray = [];

  //add index to item
  for (let i = 0; i < resGetReports.length; i++) {
    const obj = {
      index: i,
      ...resGetReports[i]._doc
    };
    finalArray.push(obj);
  }

  //send them to client
  res.status(200).json(finalArray);
});

module.exports = router;
