const express = require("express");
const router = express.Router();
const checkToken = require("app/middleware/checkMemberToken");
const Discount = require("app/models/discount");
const disComment = require("app/models/disComment");
const mongoose = require("mongoose");
const getTime = require("app/constants/getTime");
//todo: check if owner is active

router.post("/", checkToken, async (req, res) => {
  console.log("========== Member Post Comment ===========");
  //get parameters
  const { cmtText, cmtTo, cmtRating } = req.body;
  const { _id: cmtFrom } = req.tokenInfo;

  //check if request is good
  if (!cmtText || !cmtTo || !cmtRating)
    return res.status(500).json({ CODE: 5010 });

  //log
  console.log({ cmtText, cmtTo, cmtRating, cmtFrom });

  //get discount
  const resGetDiscount = await Discount.findOne({
    _id: mongoose.Types.ObjectId(cmtTo),
    isActive: true
  });

  //log
  console.log({ resGetDiscount });

  //check if discount exists
  if (!resGetDiscount) return res.status(500).json({ CODE: 5011 });

  //check if comment is valid
  const resGetComment = await disComment.findOne({
    cmtFrom,
    cmtTo,
    cmtStatus: "Waiting"
  });

  if (resGetComment) return res.status(500).json({ CODE: 5012 });

  //add comment to database
  const resAddComment = await new disComment({
    cmtText,
    cmtTime: getTime(),
    cmtTo: mongoose.Types.ObjectId(cmtTo),
    cmtRating,
    cmtFrom
  }).save();

  //send result to the client
  res.status(200).json({ msg: "Added" });
});

router.get("/:disId/:page", checkToken, async (req, res) => {
  console.log("=========== Member Get Comments ==========");

  //get param
  const { disId: cmtTo, page } = req.params;

  //get comments
  const resGetComments = await disComment.paginate(
    { cmtTo, cmtStatus: "Accepted" },
    { page, limit: 6 }
  );

  //send the result to the client
  res.status(200).json(resGetComments);
});

router.delete("/:cmtId", checkToken, async (req, res) => {
  console.log("=========== Member Delete Comment ==========");

  //get param
  const { cmtId } = req.params;

  //delete comment
  const resDeleteComment = await disComment.deleteOne({
    _id: mongoose.Types.ObjectId(cmtId)
  });

  //send the result to the client
  res.status(200).json({ CODE: 1029 });
});

module.exports = router;
