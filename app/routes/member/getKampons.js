const express = require("express");
const router = express.Router();
const Order = require("app/models/order");
const checkToken = require("app/middleware/checkMemberToken");
const callPython = require("app/constants/callPython");

router.get("/", checkToken, async (req, res, next) => {
  console.log(
    "\n============================== Member Get Kampons =============================="
  );

  callPython("generateKampons.py", req.tokenInfo._id, async result => {
    res.status(200).json(JSON.parse(result));
  });
});

module.exports = router;
