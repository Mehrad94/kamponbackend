const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const Order = require("app/models/order");
const checkToken = require("app/middleware/checkMemberToken");
const strings = require("app/values/strings");
const mongoose = require("mongoose");
const callPython = require("app/constants/callPython");

router.get("/", checkToken, async (req, res) => {
  console.log(
    "======================= Member Get Profile ======================="
  );

  console.log({ tokenInfo: req.tokenInfo });

  const { mPhoneNumber, _id: oMember } = req.tokenInfo;

  let resGetMember = await Member.findOne({ mPhoneNumber });

  if (!resGetMember) return res.status(500).json({ CODE: strings.BAD_REQUEST });

  const resGetOrders = await Order.find({
    oMember: mongoose.Types.ObjectId(oMember)
  });
  console.log({ resGetOrders });

  const ordersCount = resGetOrders.length;
  console.log({ ordersCount });

  const resGetUsedOrders = await Order.find({
    oMember: mongoose.Types.ObjectId(oMember),
    isUsed: true
  });
  console.log({ resGetUsedOrders });

  const usedOrdersCount = resGetUsedOrders.length;
  console.log({ usedOrdersCount });

  resGetMember["ordersCount"] = ordersCount;
  resGetMember["usedOrdersCount"] = usedOrdersCount;

  const profile = {
    profileInfo: resGetMember,
    ordersCount,
    usedOrdersCount
  };

  res.status(200).json(profile);
});

router.put("/", checkToken, async (req, res) => {
  console.log(
    "======================= Member Post Profile ======================="
  );

  //log
  console.log({ tokenInfo: req.tokenInfo });

  //get info from token
  const { mPhoneNumber, _id } = req.tokenInfo;
  console.log({ mPhoneNumber });

  //get params
  const params = req.body;
  console.log({ params });

  //collection name
  const collectionName = "members";

  //ask python to update the collection ==> stringify the java script object and send it to python
  callPython(
    "editor.py",
    JSON.stringify({ collectionName, _id, params }),
    async result => {
      res.status(200).json({ CODE: result });
    }
  );
});

module.exports = router;
