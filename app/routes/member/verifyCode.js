const express = require("express");
const router = express.Router();
const mLogin = require("app/models/logins/mLogin");
const Member = require("app/models/member");
const jwt = require("jsonwebtoken");
const securityConstants = require("app/constants/Security");

router.post("/", async (req, res) => {
  console.log("=============== Member Verify Code ==================");
  //object destructuring
  const { mPhoneNumber, mCode } = req.body;

  //log
  console.log({ mPhoneNumber, mCode });

  //check if mPhoneNumber or mCode are not empty
  if (!req.body.mPhoneNumber || !req.body.mCode)
    return res.status(500).json({ ERROR: 1011 });

  //check if code is ok
  const resGetmLogin = await mLogin.findOne({ mPhoneNumber, mCode });
  console.log({ resGetmLogin });

  if (!resGetmLogin) return res.status(500).json({ ERROR: 1014 });
  else await mLogin.deleteOne({ mPhoneNumber, mCode });

  //insert him to member collection and use info in jwt
  const resAddMember = await Member.findOneAndUpdate(
    { mPhoneNumber },
    { mPhoneNumber, isNewMember: false, mIsActive: true },
    { new: true, upsert: true }
  );
  console.log({ resAddMember });

  if (!resAddMember) return res.status(500).json({ CODE: 1010 });

  //Generate Token
  const token = jwt.sign(
    {
      mPhoneNumber,
      _id: resAddMember._id,
      isAdmin: false,
      isOwner: false,
      isMember: true
    },
    securityConstants.tokensKey,
    { expiresIn: "9000h" }
  );

  console.log({ token });
  //Send the result
  res.status(200).json({ token });
});

module.exports = router;

//1013 : badRequest
