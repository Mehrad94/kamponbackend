const express = require("express");
const router = express.Router();
const Discount = require("../models/discount");
const Owner = require("../models/owner");
const categoryModal = require("../models/category");
const security = require("app/constants/Security");
const mongoose = require("mongoose");

//------------------------------------------- Get all discount
router.get("/:category/:page", async (req, res) => {
  const disCategory = req.params.category;
  const disPage = req.params.page;
  // Joi Validate
  const joiValidationResult = security.validate({
    category: disCategory,
    pageNumber: disPage
  });
  if (joiValidationResult.error) {
    return res
      .status(500)
      .json({ error: joiValidationResult.error.details[0].message });
  }
  // check if this cat exists in db
  const checkIfThisCatExists = await categoryModal.findOne({
    catTitle: disCategory
  });

  if (!checkIfThisCatExists) {
    return res.status(500).json({ error: disCategory + " doesn't exist" });
  }

  //check if the requested discount is for main page
  if (disCategory === "main") {
    const allDiscounts = await Discount.paginate(
      { isActive: true },
      { page: disPage, limit: 6 }
    );
    res.status(200).json(allDiscounts);
  }
  //if the requested discounts are not for main page
  else {
    const getAllDiscountInOneCategory = await Discount.paginate(
      { disCategory, isActive: true },
      { page: disPage, limit: 6 }
    );
    console.log({ getAllDiscountInOneCategory });
    if (getAllDiscountInOneCategory)
      return res.status(200).json(getAllDiscountInOneCategory);
    else return res.status(500).json({ err: "SOME ERROR" });
  }
});

//------------------------------------------- Get  discount by id
router.get("/:discountId", async (req, res) => {
  console.log(
    "================== Member Get Discount By Id ====================="
  );

  //get id from url params
  const discountId = req.params.discountId;

  //log
  console.log({ discountId });

  //validate mongooseId
  if (!security.validateMongooseId(discountId))
    return res.status(500).json({ error: "this id is NOT valid" });

  try {
    //get discount
    const resFindDiscount = await Discount.findOne({
      _id: mongoose.Types.ObjectId(discountId)
    }).populate("ownerId");

    //log
    console.log({ resFindDiscount });

    //check if discount exists
    if (!resFindDiscount)
      return res
        .status(500)
        .json({ CODE: 1059, msg: "Discount doesnt exists" });

    //get owner
    const disOwner = await Owner.findOne({ _id: resFindDiscount.ownerId });

    //log
    console.log({ disOwner });

    //check if owner exists
    if (!disOwner)
      return res.status(500).json({ CODE: 1059, msg: "Owner doesnt exists" });

    //add the viewCount of this discount
    await Discount.findOneAndUpdate(
      { _id: discountId },
      { $inc: { viewCount: 1 } },
      { new: true }
    );

    //make newDiscount Object with whole owner object in it (we didnt populate ownerId beacause of front end guys!)
    const discount = { ...resFindDiscount._doc, disOwner };

    //log
    console.log({ discount });

    //send the result to the client
    res.status(200).json(discount);
  } catch (e) {
    console.log({ e });
    res.status(500).json(e);
  }
});

//------------------------------------------- Get all discount based on member id
router.get("disByOwner/:ownerId/:page", async (req, res) => {
  const { ownerId, page } = req.params;

  //if the requested discounts are not for main page
  const getDiscountByOwner = await Discount.paginate(
    { ownerId, isActive: true },
    { page, limit: 6 }
  );
  console.log({ getDiscountByOwner });
  if (getDiscountByOwner) return res.status(200).json(getDiscountByOwner);
  else return res.status(500).json({ err: "SOME ERROR" });
});
module.exports = router;
