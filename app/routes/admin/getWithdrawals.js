const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Withdrawals = require("app/models/withdrawalRequest");

router.get("/:status", checkAdminAuth, async (req, res) => {
  console.log("================== Admin get Transactions ==================");

  //get status from url
  const { status } = req.params;

  //log
  console.log({ status });

  //get requested withdrawals
  const resGetWithdrawals = await Withdrawals.find({ status }).populate(
    "owner"
  );

  //log
  console.log({ resGetWithdrawals });

  //send the result to the client
  res.status(200).json(resGetWithdrawals);
});

module.exports = router;
