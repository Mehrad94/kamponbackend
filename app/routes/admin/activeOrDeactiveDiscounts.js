const express = require("express");
const router = express.Router();
const Discount = require("app/models/discount");
const mongoose = require("mongoose");
const checkAdminAuth = require("app/middleware/checkAdminToken");

router.post("/", checkAdminAuth, async (req, res, next) => {
  console.log(
    "================ Admin Deactive/Active Discount ================"
  );

  //get discount id from body
  const { discountId } = req.body;

  try {
    //get current status of this discoun
    const { isActive } = await Discount.findOne({
      _id: mongoose.Types.ObjectId(discountId)
    });

    //update discount
    const resUpdateDiscount = await Discount.findOneAndUpdate(
      {
        _id: mongoose.Types.ObjectId(discountId)
      },
      { isActive: !isActive }
    );

    //return an error if failed
    if (!resUpdateDiscount) return res.status(500).json({ ERROR: 1034 });

    //log
    console.log({ ERROR: 1034 });

    //send the result to the client
    res.status(200).json({ CODE: 2034 });
  } catch (e) {
    //log
    console.log({ ERROR: 1034 });

    //log
    console.log({ e });
    //return an error if failed
    res.status(500).json({ ERROR: 1034 });
  }
});

module.exports = router;
