const express = require("express");
const getTime = require("app/constants/getTime");
const router = express.Router();
const mongoose = require("mongoose");
const withdrawalRequest = require("app/models/withdrawalRequest");
const oTransaction = require("app/models/ownersTransaction");

router.post("/", async (req, res) => {
  console.log(
    "===================== Reply To Withdrawal ====================="
  );

  //get neccesarry fields from req.body
  const { requestId, answer } = req.body;

  //log
  console.log({ requestId, answer });

  try {
    //modify data in database
    const resModifyRequest = await withdrawalRequest.findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(requestId) },
      { status: answer },
      { new: true }
    );

    //log
    console.log({ resModifyRequest });

    //if modification failed send 500 error
    if (!resModifyRequest) return res.status(500).json({ ERROR: 1084 });

    //if Accepted
    if (resModifyRequest.status === "Accepted") {
      //generate new transaction object
      const objTransaction = {
        otType: "WITHDRAWAL",
        otAmount: Number(resModifyRequest.amount) * -1,
        otDescription: "تصفیه حساب",
        otOwner: resModifyRequest.owner,
        otTime: getTime()
      };

      //log
      console.log({ objTransaction });

      //save transaction in database
      const resAddTransaction = await new oTransaction(objTransaction).save();

      //log
      console.log({ resAddTransaction });

      //log
      console.log({ CODE: 2084 });

      //return success code to the cliet
      return res.status(200).json({ CODE: 2084 });
    }

    if (resModifyRequest.status === "Rejected") {
      //log
      console.log({ CODE: 2083 });

      //send the result to the client
      return res.status(500).json({ CODE: 2083 });
    }

    //log
    console.log({ ERROR: 1084 });

    //send an error to the client
    res.status(500).json({ ERROR: 1084 });
  } catch (e) {
    //send an error to the client
    res.status(500).json({ ERROR: 1084 });
  }
});

module.exports = router;
