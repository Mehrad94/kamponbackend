const express = require("express");
const router = express.Router();
const Slide = require("app/models/slide");
const Discount = require("app/models/discount");
const checkAdminAuth = require("app/middleware/checkAdminToken");
const imgModel = require("app/models/image");
const mongoose = require("mongoose");
const callPython = require("./../../constants/callPython");

//------------------------------------------- Get slide by type
router.get("/:sType", checkAdminAuth, async (req, res) => {
  console.log("============ Admin Get Slides ==============");

  //get sType from req.params.sType
  const sType = req.params.sType;
  console.log({ sType });

  //call python to get all slides with slideId Lable
  callPython("getSliders.py", sType, result => {
    //parse the retrieved json string and send it to the client
    res.status(200).json(JSON.parse(result));
  });
});

//------------------------------------------- Add new category
router.post("/", checkAdminAuth, async (req, res) => {
  console.log("===================== Admin Add Slide =====================");
  const { sType, sImage, sDiscountId } = req.body;
  console.log({ sType, sImage, sDiscountId });

  //get Owner from sDiscountId
  const resGetDiscount = await Discount.findOne({
    _id: mongoose.Types.ObjectId(sDiscountId)
  });

  //no discount with this id exists
  if (!resGetDiscount) return res.status(500).json({ CODE: 1037 });

  //get owner
  const sOwner = resGetDiscount.ownerId;

  // make new slide
  const slide = await new Slide({
    sType,
    sImage,
    sDiscountId: mongoose.Types.ObjectId(sDiscountId),
    sOwner
  });
  slide
    .save()
    .then(() => {
      res.status(201).json({ message: "Slide Created" });
    })
    .catch(err => res.status(500).json({ err }));
});

//------------------------------------------- Delete discount by id
router.delete("/:slideId", checkAdminAuth, async (req, res) => {
  console.log("===================== Admin Delete Slide =====================");

  const _id = req.params.slideId;
  console.log({ _id });

  const resDeleteSlide = await Slide.deleteOne({
    _id: mongoose.Types.ObjectId(_id)
  });

  console.log({ resDeleteSlide });
  res.status(200).json({ resDeleteSlide });
});

module.exports = router;
