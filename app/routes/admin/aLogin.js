const express = require("express");
const router = express.Router();
const Admin = require("app/models/admin");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const security = require("app/constants/Security");

// router.post("/", async (req, res) => {
//   const { aPhoneNumber, aPassword } = req.body;
//   let hash = bcrypt.hashSync(aPassword, 15);
//   if (hash) {
//     const newAdmin = new Admin({ aPhoneNumber, aPassword: hash });
//     newAdmin
//       .save()
//       .then(() => {
//         res.status(200).json({ msg: "Admin Created" });
//       })
//       .catch(() => {
//         res.status(500).json({
//           error: "Failed to create admin"
//         });
//       });
//   } else {
//     res.status(500).json({ error: "Hashing Process Failed" });
//   }
// });

// router.post("/", async (req, res) => {
//   const { aPhoneNumber, aPassword } = req.body;
//   const resGetAdmin = await Admin.findOne({ aPhoneNumber, aPassword });
//   if (resGetAdmin) {
//     bcrypt.compare(aPassword, resGetAdmin.aPassword, async (err, result) => {
//       if (err) return res.status(500).json({ ER: "Auth failed" });
//       const token = jwt.sign({ aPhoneNumber }, securityConstants.tokensKey, {
//         expiresIn: "9000h"
//       });
//       const resSaveToken = await Admin.findOneAndUpdate(
//         { aPhoneNumber },
//         { $set: { aToken: token } },
//         { new: true }
//       );
//       if (resSaveToken) return res.status(200).json({ token });
//       else return res.status(500).json({ err: "LOGIN FAILED" });
//     });
//   } else res.status(500).json({ ER: "Auth failed" });
// });

router.post("/", async (req, res) => {
  const { aPhoneNumber, aPassword } = req.body;
  const resGetAdmin = await Admin.findOne({ aPhoneNumber });
  if (resGetAdmin) {
    const isPasswordOK = security.isHashValid(aPassword, resGetAdmin.aPassword);
    if (isPasswordOK) {
      const token = jwt.sign({ aPhoneNumber }, securityConstants.tokensKey, {
        expiresIn: "9000h"
      });
      res.status(200).json({ token });
    } else {
      res.status(500).json({ CODE: 1010 });
    }
  } else {
    res.status(500).json({ CODE: 1010 });
  }
});

module.exports = router;

//1010 ==> Wrong Pass
