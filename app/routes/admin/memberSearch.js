const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Member = require("app/models/member");

router.post("/", checkAdminAuth, async (req, res, next) => {
  //log
  console.log("================== Search Member ==================");

  //get search string
  const { searchString } = req.body;

  //log
  console.log({ searchString });

  //declare a seach result array
  let searchResult = [];

  //get all owners from data base
  const resGetAllMembers = await Member.find();

  //for trough all Members
  for (let i = 0; i < resGetAllMembers.length; i++) {
    //if oName or oPhoneNumbers includes search string
    if (
      resGetAllMembers[i].mName.includes(searchString) ||
      resGetAllMembers[i].mPhoneNumber.includes(searchString)
    )
      //push the current owner object in searchReasult array
      searchResult.push(resGetAllMembers[i]);

    //if length of searched array reached 3 break the for loop
    if (searchResult.length === 3) break;
  }

  //log
  console.log({ searchResult });

  //send search result array to the client
  res.status(200).json(searchResult);
});

module.exports = router;
