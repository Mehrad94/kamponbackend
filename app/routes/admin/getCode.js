const express = require("express");
const router = express.Router();
const aLogin = require("app/models/logins/aLogin");
const Admin = require("app/models/admin");
const Security = require("app/constants/Security");
const { sendLoginSMS } = require("app/constants/sendSMS");

router.post("/", async (req, res) => {
  const { aPhoneNumber } = req.body;

  //check if aPhoneNumber is full
  if (!aPhoneNumber) return res.status(500).json({ ERROR: 1011 });

  //check if this phone number exists in Admin Collection
  const resGetAdmin = await Admin.findOne({ aPhoneNumber });
  if (!resGetAdmin) return res.status(500).json({ ERROR: 1016 });

  //generate a random code
  const aCode = Security.generateRandomNumber(4);

  //update or insert temperorly in aLogins collection
  const resAddNewAdminLogin = await aLogin.findOneAndUpdate(
    {
      aPhoneNumber
    },
    { $set: { aToken: aCode, aPhoneNumber } },
    { upsert: true, new: true }
  );

  //check if the operation was succesfull
  if (!resAddNewAdminLogin) return res.status(500).json({ ERROR: 1050 });

  //send sms
  await sendLoginSMS(aPhoneNumber, aCode, res);
});

module.exports = router;

// CODE: 1016 ==> admin doesnt exists
