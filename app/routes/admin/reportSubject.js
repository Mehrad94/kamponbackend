const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const ReportSubject = require("app/models/reportSubject");
const mongoose = require("mongoose");

router.get("/", checkAdminAuth, async (req, res) => {
  console.log("\n\n=============== Admin Get Reports Items ===============");

  //find all reports from database
  const resGetReports = await ReportSubject.find();

  //send them to client
  res.status(200).json(resGetReports);
});

router.post("/", checkAdminAuth, async (req, res) => {
  console.log("=========== Admin Add Report ==========");

  try {
    const { subject } = req.body;

    console.log({ body: req.body });

    //check if requierd fields are filled
    if (!subject) return res.status(500).json({ ERROR: 5010 });

    //add report
    const resAddReport = await new ReportSubject({
      subject
    }).save();

    res.status(200).json({ CODE: 2026 });
  } catch (error) {
    console.log({ error });
    res.status(500).json({ ERROR: 1010 });
  }
});

router.delete("/:itemId", async (req, res) => {
  console.log("=========== Admin Delete Report Subject ===========");
  try {
    const { itemId: _id } = req.params;
    console.log({ body: req.body });
    console.log({ headers: req.headers });
    console.log({ params: req.parsams });

    //find all reports from database
    const resDeleteReports = await ReportSubject.deleteOne({
      _id: mongoose.Types.ObjectId(_id)
    });

    console.log({ resDeleteReports });

    //send them to client
    res.status(200).json({ CODE: 2012 });
  } catch (error) {
    res.status(500).json({ ERROR: 1019 });
  }
});

module.exports = router;
