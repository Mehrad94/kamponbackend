const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Owner = require("app/models/owner");

router.post("/", checkAdminAuth, async (req, res, next) => {
  //log
  console.log("================== Search Owner ==================");

  //get search string
  const { search } = req.body;

  //log
  console.log({ search });

  //declare a seach result array
  let searchResult = [];

  //get all owners from data base
  const resGetAllOwners = await Owner.find();

  //for trough all owners
  for (let i = 0; i < resGetAllOwners.length; i++) {
    //if oName or oPhoneNumbers includes search string
    if (
      resGetAllOwners[i].oName.includes(search) ||
      resGetAllOwners[i].oPhoneNumber.includes(search)
    )
      //push the current owner object in searchReasult array
      searchResult.push(resGetAllOwners[i]);

    //if length of searched array reached 3 break the for loop
    if (searchResult.length === 3) break;
  }

  //log
  console.log({ searchResult });

  //send search result array to the client
  res.status(200).json(searchResult);
});

module.exports = router;
