const express = require("express");
const router = express.Router();
const Report = require("app/models/report");
const checkAdminAuth = require("app/middleware/checkAdminToken");

router.get("/", checkAdminAuth, async (req, res) => {
  console.log("\n\n=============== Admin Get Reports ===============");

  //find all reports from database
  const resGetReports = await Report.find().populate("disId memberId");

  //send them to client
  res.status(200).json(resGetReports);
});

module.exports = router;
