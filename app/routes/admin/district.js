const express = require("express");
const router = express.Router();
const district = require("app/models/district");
const checkAdminAuth = require("app/middleware/checkAdminToken");
const mongoose = require("mongoose");

router.get("/", checkAdminAuth, async (req, res) => {
  console.log("========= Admin Get Districts =========");

  //get districts from data base
  const resgetDistricts = await district.find();

  //log
  console.log({ resgetDistricts });

  //send result to the client
  res.status(200).json(resgetDistricts);
});

router.post("/", checkAdminAuth, async (req, res) => {
  console.log("========= Admin Add District =========");

  //get district name from req.body
  const { districtName } = req.body;

  //log
  console.log({ districtName });

  //add a new district
  const resAddDistricts = await new district({
    districtName
  }).save();

  //send result to the client
  console.log({ CODE: 2009 });

  //send result to the client
  res.status(200).json({ CODE: 2009 });
});

router.delete("/:districtID", checkAdminAuth, async (req, res) => {
  console.log("========= Admin delete District =========");
  const { districtID: _id } = req.params;

  //delete district
  const resDeleteDistrict = await district.deleteOne({
    _id: mongoose.Types.ObjectId(_id)
  });

  //log
  console.log({ CODE: 2069 });

  //send the result to the client
  res.status(200).json({ CODE: 2069 });
});

module.exports = router;

// CODE: 1016 ==> admin doesnt exists
