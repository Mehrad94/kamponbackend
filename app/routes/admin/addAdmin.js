const express = require("express");
const router = express.Router();
const Admin = require("app/models/admin");
const checkAdminAuth = require("app/middleware/checkAdminToken");

router.post("/",checkAdminAuth, async (req, res) => {
  const { aPhoneNumber } = req.body;

  const resAddAdmin = new Admin({ aPhoneNumber }).save();
  if (resAddAdmin) {
    res.status(200).json({ msg: "Admin Created" });
  } else {
    res.status(500).json({ msg: "Failed" });
  }
});

module.exports = router;

// CODE: 1016 ==> admin doesnt exists
