const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const aLogin = require("app/models/logins/aLogin");
const Admin = require("app/models/admin");
const securityConstants = require("app/constants/Security");

router.post("/", async (req, res) => {
  //check if aPhoneNumber or aCode are not empty
  if (!req.body.aPhoneNumber || !req.body.aCode)
    return res.status(500).json({ CODE: 1011 });

  //object destructuring
  const { aPhoneNumber, aCode } = req.body;
  console.log({ aPhoneNumber, aCode });

  //Check if this admin exists
  const resGetAdmin = await Admin.findOne({ aPhoneNumber });
  if (!resGetAdmin) return res.status(500).json({ CODE: 1014 });

  //increase tried time
  const resUpdateTriedTimes = await aLogin.findOneAndUpdate(
    { aPhoneNumber },
    { $inc: { triedTimes: 1 } }
  );

  //log
  console.log({ resUpdateTriedTimes });

  //check if he tried more than 5 times
  if (resUpdateTriedTimes && resUpdateTriedTimes.triedTimes >= 5) {
    console.log("Wrong Attemp Nor then 5 times ...");
    const resExpireACode = await aLogin.deleteOne({
      aPhoneNumber
    });
    return res.status(500).json({ ERROR: 1037 });
  }

  //check if aCode is true
  const resGetAlogin = await aLogin.findOne({ aPhoneNumber, aToken: aCode });
  if (!resGetAlogin) return res.status(500).json({ CODE: 1012 });

  //Generate Token
  const token = jwt.sign(
    {
      aPhoneNumber,
      _id: resGetAdmin._id,
      isAdmin: true,
      isOwner: false,
      isMember: false
    },
    securityConstants.tokensKey,
    { expiresIn: "9000h" }
  );

  console.log({ sentToken: token });

  //delete aCode
  const resDeleteAlogin = await aLogin.deleteOne({
    aPhoneNumber,
    aToken: aCode
  });

  //Send the token
  res.status(200).json({ token });
});

module.exports = router;

//1013 : badRequest
