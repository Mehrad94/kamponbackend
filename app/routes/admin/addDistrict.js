const express = require("express");
const router = express.Router();
const district = require("app/models/district");
const checkAdminAuth = require("app/middleware/checkAdminToken");

router.post("/", checkAdminAuth, async (req, res) => {
  console.log("========= Admin Add District =========");
  const resAddDistricts = await new district({
    name: req.body.districtName
  }).save();
  res.status(200).json({ CODE: 2009 });
});

module.exports = router;

// CODE: 1016 ==> admin doesnt exists
