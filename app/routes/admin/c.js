const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Category = require("app/models/category");
const imgModel = require("app/models/image");
const callPython = require("app/constants/callPython");
const strings = require("app/values/strings");
const mongoose = require("mongoose");

//------------------------------------------- Get all category
router.get("/", checkAdminAuth, async (req, res) => {
  callPython("getCategories.py", "", async result => {
    const cats = JSON.parse(result);
    console.log({ cats });

    res.status(200).json(cats);
  });
});

//------------------------------------------- Add new category
router.post("/", checkAdminAuth, async (req, res) => {
  console.log("================= Add Catrgory ====================");
  console.log({ catTitle: req.body.catTitle, catImage: req.body.catImage });

  const { catTitle, catImage, catPersianTitle } = req.body;

  // add new Category
  const category = new Category({ catTitle, catImage, catPersianTitle });
  category
    .save()
    .then(() => {
      res.status(201).json({ message: "Category Created" });
    })
    .catch(err => res.status(500).json({ err }));
});

// ========================= get All SubCategories =======================
router.get("/:catId", async (req, res) => {
  try {
    console.log(" ================= Member Get Subcategories ==============");
    console.log({ params: req.params });
    console.log({ body: req.body });
    console.log({ headers: req.headers });
    const resGetAllCats = await Category.findOne({
      _id: mongoose.Types.ObjectId(req.params.catId)
    });
    res.send(resGetAllCats.subCategories);
  } catch (e) {
    console.log({ e });
  }
});

//------------------------------------------- Add new sub category
router.post("/subCategory/:catId", checkAdminAuth, async (req, res) => {
  try {
    console.log("================= Admin Add Subcatrgory ====================");

    const { subCategoriesTitle } = req.body;
    const { catId: _id } = req.params;

    //check if cat id is valid
    const resFindCategory = await Category.findOne({
      _id: mongoose.Types.ObjectId(_id)
    });

    if (!resFindCategory) {
      console.log("invalid cat Id: " + _id);
      return res.status(500).json({ CODE: strings.BAD_REQUEST });
    }

    //check if this sub category is not repeatetive
    const { subCategories } = await Category.findOne({
      _id: mongoose.Types.ObjectId(_id)
    });

    if (subCategories.indexOf(subCategoriesTitle) > -1) {
      console.log("Repeatetive sub Category: " + subCategoriesTitle);
      return res.status(500).json({ CODE: strings.BAD_REQUEST });
    }

    //add sub category
    const resUpdateCategory = await Category.findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(_id) },
      { $addToSet: { subCategories: subCategoriesTitle } },
      { new: true }
    );
    console.log({ resUpdateCategory });
    res.status(200).json({ CODE: 2027 });
  } catch (error) {
    console.log("Catched ...");
    res.status(500).json({ ERROR: 1010 });
  }
});

//------------------------------------------- Delete category by id
router.delete("/:categoryId", (req, res) => {
  const _id = req.params.categoryId;
  Category.remove({ _id })
    .exec()
    .then(() => {
      res.status(200).json({ message: "Category Deleted" });
    })
    .catch(err => res.status(500).json({ err }));
});

//------------------------------------------- edit category by id
router.put("/:catId", async (req, res, next) => {
  //get id from url
  const _id = req.params.catId;

  //specify collection name
  const collectionName = "categories";

  //get params
  const params = req.body;

  //log
  console.log({ params });

  //ask python to update the collection ==> stringify the java script object and send it to python
  callPython(
    "editor.py",
    JSON.stringify({ collectionName, _id, params }),
    async result => {
      res.status(200).json({ CODE: strings.SUCCESS_UPDATED });
    }
  );
});

module.exports = router;
