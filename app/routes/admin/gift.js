const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Discount = require("app/models/discount");
const Member = require("app/models/member");
const Order = require("app/models/order");
const getTime = require("app/constants/getTime");
const Security = require("app/constants/Security");
const MTransaction = require("app/models/memberTransaction");
const OTransaction = require("app/models/ownersTransaction");

router.post("/:disId/:memberId/:count", checkAdminAuth, async (req, res) => {
  console.log("============= Admin Add Gift ============");

  try {
    const { disId, memberId, count } = req.params;
    const time = getTime();

    //get discount
    const resGetDiscount = await Discount.findOne({
      _id: mongoose.Types.ObjectId(disId)
    });

    console.log({ resGetDiscount });
    //get member
    const resGetMember = await Member.findOne({
      _id: mongoose.Types.ObjectId(memberId)
    });

    console.log({ resGetMember });

    //check if id's were correct
    if (!resGetDiscount || !resGetMember || !count) {
      return res.status(500).json({ ERROR: 5010 });
    }

    for (let i = 0; i < Number(count); i++) {
      console.log({ i });
      //make a new order
      const newOrder = {
        number: i,
        time,
        disId,
        oMember: resGetMember._id,
        oTitle: resGetDiscount.disTitle,
        oDescription: resGetDiscount.disDescription,
        oImages: resGetDiscount.disImages,
        oPercent: resGetDiscount.disPercent,
        oRealPrice: resGetDiscount.disRealPrice,
        oNewPrice: resGetDiscount.disNewPrice,
        oCategory: resGetDiscount.disCategory,
        oCategoryPersian: resGetDiscount.disCategoryPersian,
        oOwner: resGetDiscount.ownerId,
        oDistrict: resGetDiscount.disDistrict,
        oFeatures: resGetDiscount.disFeatures,
        oTermsOfUse: resGetDiscount.disTermsOfUse,
        oAuthority: "GIFT",
        oIsGift: true,
        trackCode: Security.IDGenerator()
      };

      console.log({ newOrder });
      //add it to database
      const resAddOrder = await new Order(newOrder).save();
      console.log({ resAddOrder });

      //make new transaction for member
      const mTransaction = {
        mtTime: time,
        mtOwner: resGetDiscount.ownerId,
        mtAmount: resGetDiscount.disNewPrice,
        mtDescription: resGetDiscount.disTitle + "خرید بابت",
        mtAuthority: "GIFT"
      };
      console.log({ mTransaction });

      //add transaction todatabase
      const resAddNewMemberTransactionToDb = await new MTransaction(
        mTransaction
      ).save();
      console.log({ resAddNewMemberTransactionToDb });

      //make new transaction for owner
      const oTransaction = {
        otTime: time,
        otOwner: resGetDiscount.ownerId,
        otAmount: resGetDiscount.disNewPrice,
        otDescription: " خرید بابت " + resGetDiscount.disTitle,
        otAuthority: "GIFT"
      };
      console.log({ oTransaction });

      //add transaction todatabase
      const resAddNewOwnerTransactionToDb = await new OTransaction(
        oTransaction
      ).save();
      console.log({ resAddNewOwnerTransactionToDb });
    }

    res.status(200).json({ CODE: 2014 });
  } catch (error) {
    console.log({ error });
    res.status(500).json({ ERROR: 1014 });
  }
});

router.get("/:page", checkAdminAuth, async (req, res) => {
  console.log("========= Admin Get Gifts ==========");
  const { page } = req.params;

  console.log({ page });

  const getAllGets = await Order.paginate(
    { oIsGift: true },
    { page, limit: 6, populate: ["oMember oOwner"] }
  );

  if (getAllGets) return res.status(200).json(getAllGets);
  else return res.status(500).json({ ER: "Some error happen" });
});
module.exports = router;
