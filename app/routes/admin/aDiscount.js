const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Discount = require("app/models/discount");

router.get("/:page", checkAdminAuth, async (req, res) => {
  const disPage = req.params.page;
  const getAllDiscount = await Discount.paginate(
    {},
    { page: disPage, limit: 6 }
  );
  if (getAllDiscount) return res.status(200).json(getAllDiscount);
  else return res.status(500).json({ er: "Some error happen" });
});

module.exports = router;
