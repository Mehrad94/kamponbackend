const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const imgModel = require("app/models/image");
const fs = require("fs");

//decelare a deleted count to 0 and ++ it while deleting
deletedCount = 0;

router.get("/", checkAdminAuth, async (req, res, next) => {
  //get all not used image names from db
  const resGetNotUsedImages = await imgModel.find({ isUsed: false });

  //for in retrieved images from db
  for (let i = 0; i < resGetNotUsedImages.length; i++) {
    //delete image from uploads folder
    await fs.unlink("uploads/" + resGetNotUsedImages[i].iName, async e => {
      //log error if exists
      if (e) console.log({ e });

      //delete image from db
      await imgModel.remove({ iName: resGetNotUsedImages[i].iName });

      // ++ deleted count
      deletedCount = deletedCount + 1;
      console.log({ deletedCount });
    });
  }

  //send response to client
  res.status(200).json({ msg: deletedCount + " files deleted" });
});

module.exports = router;
