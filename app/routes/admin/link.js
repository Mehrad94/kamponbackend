const express = require("express");
const router = express.Router();
const Link = require("app/models/link");
const checkAdminAuth = require("app/middleware/checkAdminToken");
const strings = require("app/values/strings");

router.get("/", checkAdminAuth, async (req, res) => {
  console.log("\n\n=============== Admin Get Links ===============");

  //find all linkes from database
  const resGetLinks = await Link.find();

  //send them to client
  res.status(200).json(resGetLinks);
});

router.post("/", checkAdminAuth, async (req, res) => {
  console.log("\n\n=============== Admin Add New Link ===============");
  //check if fields are filled
  if (!req.body.link || !req.body.title)
    return res.status(500).json({ CODE: strings.BAD_REQUEST });

  //get fields from req.body
  const { link, title } = req.body;

  //add this link to the database
  const ressAddLink = await new Link({ link, title }).save();

  //send the result yo the client
  res.status(200).json({ CODE: strings.SUCCESS_ADD_LINK });
});
module.exports = router;
