const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const callPython = require("app/constants/callPython");
const Member = require("app/models/member");

router.get("/", checkAdminAuth, async (req, res) => {
  const allMember = await Member.find();
  console.log({ allMember: allMember });
  if (allMember) res.status(200).json(allMember);
  else res.status(500).json({ Er: "Some error accrued" });
});

router.put("/:memberId", async (req, res, next) => {
  console.log("================ Admin Member Edit ====================");
  //get id from url
  const _id = req.params.memberId;
  console.log({ _id });
  //specify collection name
  const collectionName = "members";
  console.log({ collectionName });

  //get params
  const params = req.body;
  console.log({ params });

  //ask python to update the collection ==> stringify the java script object and send it to python
  callPython(
    "editor.py",
    JSON.stringify({ collectionName, _id, params }),
    async result => {
      res.status(200).json({ CODE: result });
    }
  );
});
module.exports = router;
