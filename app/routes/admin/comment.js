const express = require("express");
const router = express.Router();
const Comment = require("app/models/disComment");
const mongoose = require("mongoose");
const getTime = require("app/constants/getTime");

router.get("/:status/:page", async (req, res) => {
  console.log("=============== Admin Get Comments =============");

  const { status, page } = req.params;
  console.log({ status, page });
  let resGetComments = null;

  switch (status) {
    case "all":
      resGetComments = await Comment.paginate({}, { page, limit: 10 });
      res.status(200).json(resGetComments);
      break;
    case "accepted":
      resGetComments = await Comment.paginate(
        { cmtStatus: "Accepted" },
        { page, limit: 10 }
      );
      res.status(200).json(resGetComments);
      break;
    case "rejected":
      resGetComments = await Comment.paginate(
        { cmtStatus: "Rejected" },
        { page, limit: 10 }
      );
      res.status(200).json(resGetComments);
      break;
    case "deleted":
      resGetComments = await Comment.paginate(
        { cmtStatus: "Deleted" },
        { page, limit: 10 }
      );
      break;
    case "waiting":
      resGetComments = await Comment.paginate(
        { cmtStatus: "Waiting" },
        { page, limit: 10 }
      );
      res.status(200).json(resGetComments);
      break;
    default:
      res.status(500).json({ CODE: 5010 });
  }
});

router.put("/:commentId/:status", async (req, res) => {
  console.log("=============== Admin Reply Comments =============");
  const { commentId, status } = req.params;

  //check if status is valid
  const statuses = ["Waiting", "Accepted", "Rejected", "Deleted"];
  if (statuses.indexOf(status) === -1)
    return res.status(500).json({ CODE: 5010 });

  //Edit Comment
  const resEditComment = await Comment.findOneAndUpdate(
    {
      _id: mongoose.Types.ObjectId(commentId)
    },
    { cmtStatus: status }
  );

  console.log({ resEditComment });
  res.status(200).json({ CODE: 2079 });
});

router.post("/reply/:commentId", async (req, res) => {
  console.log("=============== Admin Reply Comment =============");
  const { commentId } = req.params;
  const { answer } = req.body;
  console.log({ commentId, answer });

  //Edit Comment
  const resEditComment = await Comment.findOneAndUpdate(
    {
      _id: mongoose.Types.ObjectId(commentId)
    },
    { cmtReply: { text: answer, time: getTime() } }
  );

  console.log({ resEditComment });
  res.status(200).json({ CODE: 2079 });
});

module.exports = router;

//farda ke fardas
