const express = require("express");
const router = express.Router();
const Discount = require("app/models/discount");
const category = require("app/models/category");
const Owner = require("app/models/owner");
const checkAdminAuth = require("app/middleware/checkAdminToken");
const callPython = require("app/constants/callPython");
const strings = require("app/values/strings");
const mongoose = require("mongoose");
const calcActiveDiscount = require("app/utils/calcActiveDiscount");
//---------------------------------------- Get all discount
router.get("/:page", checkAdminAuth, async (req, res) => {
  console.log("=============== Admin get All Discounts ===============");
  try {
    const disPage = req.params.page;
    console.log({ disPage });
    const getAllDiscount = await Discount.paginate(
      {},
      { page: disPage, limit: 10, sort: { createdAt: -1 }, populate: "ownerId" }
    );

    //log
    console.log({ getAllDiscount });
    if (getAllDiscount) return res.status(200).json(getAllDiscount);
    else return res.status(500).json({ ER: "Some error happen" });
  } catch (e) {
    console.log({ e });
    res.status(500).json({ e });
  }
});

//---------------------------------- Get all discount in one Category
router.get("/:category/:page", checkAdminAuth, async (req, res) => {
  const disPage = req.params.page;
  const disCategory = req.params.category;
  console.log({ disCategory, disPage });
  const getAllDiscount = await Discount.paginate(
    { disCategory },
    { page: disPage, limit: 6 }
  );
  if (getAllDiscount) return res.status(200).json(getAllDiscount);
  else return res.status(500).json({ ER: "Some error happen" });
});

router.get("/:subCategory/:page", async (req, res) => {
  const { subCategory, page } = req.params;

  console.log({ subCategory, page });
  const getAllDiscount = await Discount.paginate(
    { subCategory },
    { page, limit: 6 }
  );
  if (getAllDiscount) return res.status(200).json(getAllDiscount);
  else return res.status(500).json({ ER: "Some error happen" });
});

//------------------------------------------- Add new discount
router.post("/", async (req, res) => {
  console.log("===================== Admin Add Discount =====================");
  try {
    const {
      ownerId,
      disTitle,
      disDescription,
      subCategory,
      disImages,
      disRealPrice,
      disNewPrice,
      disDistrict,
      disFeatures,
      disTermsOfUse,
      disThumbnail
    } = req.body;

    //log
    console.log({
      ownerId,
      disTitle,
      subCategory,
      disDescription,
      disImages,
      disThumbnail,
      disRealPrice,
      disNewPrice,
      disDistrict,
      disFeatures,
      disTermsOfUse
    });

    if (
      !ownerId ||
      !disTitle ||
      !disDescription ||
      !disImages.length ||
      !disThumbnail ||
      !disRealPrice ||
      !disNewPrice ||
      !disFeatures.length ||
      !disTermsOfUse.length
    )
      return res.status(500).json({ CODE: strings.BAD_REQUEST });

    //get owner
    const resGetOwner = await Owner.findOne({
      _id: mongoose.Types.ObjectId(ownerId)
    });

    //log
    console.log({ resGetOwner });

    //if this owner was invalid
    if (!resGetOwner || !resGetOwner.oIsActive)
      return res.status(500).json({ CODE: strings.INVALID_OWNER });

    const { oCategory, oDistrict, oAddress, oCategoryPersian } = resGetOwner;
    //calculate the percent
    const disPercent = 100 - Math.ceil((disNewPrice * 100) / disRealPrice);
    console.log({ disPercent });

    //add discount
    const resAddDiscount = await new Discount({
      disTitle,
      disPercent,
      ownerId,
      disDescription,
      disImages,
      subCategory,
      disThumbnail,
      disRealPrice,
      disNewPrice,
      disCategory: oCategory,
      disCategoryPersian: oCategoryPersian,
      disDistrict: oDistrict,
      disFeatures,
      disTermsOfUse,
      disAddress: oAddress
    }).save();

    console.log({ resAddDiscount });

    const resUpdateCategory = await category.findOneAndUpdate(
      { catTitle: oCategory },
      { $inc: { activeCount: 1 } }
    );
    const resUpdateOwnerKampon = await Owner.findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(ownerId) },
      { $inc: { oActiveKampon: 1 } }
    );

    console.log({ resUpdateCategory });
    console.log({ resUpdateOwnerKampon });

    res.status(200).json({ msg: "Discount Created" });
  } catch (e) {
    console.log({ e });
    res.status(500).json({ CODE: 1094 });
  }
});

// //------------------------------------------- Delete discount by id
router.delete("/:discountId", checkAdminAuth, async (req, res) => {
  console.log("================== Admin Delete Discount ==================");
  const id = req.params.discountId;

  //log
  console.log({ id });

  //get Discount
  const resGetDiscount = await Discount.findOne({
    _id: mongoose.Types.ObjectId(id)
  });

  //return error if this discount doesnt exist
  if (!resGetDiscount) {
    console.log({ ERROR: 1046, msg: "Invalid Discount" });
    res.status(500).json({ ERROR: 1046 });
  }

  //delete discount
  const resDeleteDiscount = await Discount.deleteOne({
    _id: mongoose.Types.ObjectId(id)
  });

  //log
  console.log({ resDeleteDiscount });

  //send result to client
  res.status(200).json({ msg: "Done" });
});

router.put("/status/:discountId", async (req, res) => {
  const _id = mongoose.Types.ObjectId(req.params.discountId);

  try {
    const resToggleOwnerActivity = await Discount.findOne({ _id });
    if (!resToggleOwnerActivity)
      return res.status(500).json({ msg: "DISCOUNT_NOT_FOUND" });
    const isActive = !resToggleOwnerActivity.isActive;
    const resUpdateDiscount = await Discount.findOneAndUpdate(
      { _id },
      { $set: { isActive } }
    );
    calcActiveDiscount();
    res.status(202).json({ msg: "UPDATED" });
  } catch (error) {
    res.status(500).json({ msg: "BAD_REQUEST" });
  }
});

router.put("/:discountId", async (req, res, next) => {
  //get id from url
  const _id = req.params.discountId;

  //specify collection name
  const collectionName = "discounts";

  //get params
  const params = req.body;

  //ask python to update the collection ==> stringify the java script object and send it to python
  callPython(
    "editor.py",
    JSON.stringify({ collectionName, _id, params }),
    async result => {
      res.status(200).json({ CODE: strings.SUCCESS_UPDATED });
    }
  );
});
module.exports = router;
