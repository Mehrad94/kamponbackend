const express = require("express");
const router = express.Router();
const Settings = require("app/models/settings");

router.get("/links", async (req, res) => {
  const resGetSettings = await Settings.findOne();
  res.status(200).json(resGetSettings);
});

router.post("/links", async (req, res) => {
  console.log("============= Admin Post Link ===============");

  //log req.body
  console.log({ body: req.body });

  //get key of un-known body: returns a list of keys
  const keys = Object.keys(req.body);

  console.log({ keys });

  //   //get sLinks Object from settings database
  const resFindSetting = await Settings.findOne();

  //if there is no settings
  if (!resFindSetting) {
    //initialize settings object
    let sLinks = {};

    //modify slinks un-known field
    sLinks[keys[0]] = req.body[keys[0]];

    //add new setting to database
    const resAddSettings = await new Settings({ sLinks }).save();

    //log
    console.log({ resAddSettings });
    console.log({ CODE: 2053 });

    //send the result to the client
    res.status(200).json({ CODE: 2053 });
  } else {
    let { sLinks } = await Settings.findOne();
    sLinks[keys[0]] = req.body[keys[0]];
    const resUpdateSettings = await Settings.findOneAndUpdate(
      {},
      { sLinks },
      {
        new: true,
        upsert: true
      }
    );
    console.log({ resUpdateSettings });
    console.log({ CODE: 2053 });

    //send the result to the client
    res.status(200).json({ CODE: 2053 });
  }
});

module.exports = router;
