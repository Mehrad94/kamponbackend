const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const mTransaction = require("app/models/memberTransaction");

router.get("/", checkAdminAuth, async (req, res) => {
  console.log("================== Admin get Transactions ==================");
  const resGetTransActions = await mTransaction.find();
  console.log({ resGetTransActions });
  res.status(200).json(resGetTransActions);
});

module.exports = router;
