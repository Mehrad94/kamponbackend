const express = require("express");
const router = express.Router();
const checkAdminAuth = require("app/middleware/checkAdminToken");
const Slide = require("app/models/slide");

router.get("/", checkAdminAuth, async (req, res) => {
  const slides = await Slide.find();
  console.log({ slides });
  res.status(200).json({ slides });
});

module.exports = router;
