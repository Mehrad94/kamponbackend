const express = require("express");
const router = express.Router();
const Owner = require("app/models/owner");
const checkAdminAuth = require("app/middleware/checkAdminToken");
const strings = require("app/values/strings");
const callPython = require("app/constants/callPython");
const calcKampons = require("app/utils/calcKampons");
const calcOrders = require("app/utils/calcOrders");
const calcBalance = require("app/utils/calcBalance");
const Category = require("app/models/category");

router.get("/", checkAdminAuth, async (req, res) => {
  console.log("================ Admin Get all Owner  ======================");
  //get all owners
  const resGetOwner = await Owner.find().sort({ createdAt: -1 });
  let owners = [];
  console.log({ resGetOwner });
  for (let i = 0; i < resGetOwner.length; i++) {
    const owner = resGetOwner[i];
    console.log({ owner });
    //get balance
    const resGetBalance = await calcBalance(owner._id);
    console.log({ resGetBalance });
    //active kampons
    const activeKampons = (await calcKampons(owner._id, true)).length;
    console.log({ activeKampons });
    //deactive kampons
    const notActiveKampons = (await calcKampons(owner._id, false)).length;
    console.log({ notActiveKampons });
    //calc all kampons
    const totalKampons = activeKampons + notActiveKampons;
    console.log({ totalKampons });
    //get not used orders
    const calcNotUsedOrders = (await calcOrders(owner._id, false)).length;
    console.log({ calcNotUsedOrders });
    //get used orders
    const calcUsedOrders = (await calcOrders(owner._id, true)).length;
    console.log({ calcUsedOrders });
    //calc all orders
    const totalOrders = calcNotUsedOrders + calcUsedOrders;
    console.log({ totalOrders });
    const seller = {
      ...owner._doc,
      balance: resGetBalance,
      activeKampons,
      deactiveKampons: notActiveKampons,
      kampons: totalKampons,
      notUsedOrders: calcNotUsedOrders,
      usedOrders: calcUsedOrders,
      orders: totalOrders
    };
    console.log({ seller });
    owners.push(seller);
  }

  res.status(200).json(owners);
});

router.post("/", checkAdminAuth, async (req, res) => {
  console.log("================= Admin Add Owner =================");

  //get fields from req.body
  const {
    oPhoneNumber,
    oOtherNumbers,
    oName,
    oAddress,
    oDistrict,
    oCategory,
    oAvatar
  } = req.body;

  //log
  console.log({
    oPhoneNumber,
    oOtherNumbers,
    oName,
    oAddress,
    oDistrict,
    oCategory,
    oAvatar
  });

  //check if all fields are filled
  if (
    !oPhoneNumber ||
    !oName ||
    !oAddress ||
    !oDistrict ||
    !oCategory ||
    !oAvatar
  )
    return res.status(500).json({ CODE: strings.BAD_REQUEST });

  //get persian category
  const resGetCategory = await Category.findOne({ catTitle: oCategory });
  console.log({ resGetCategory });

  //return error if category was invalid
  if (!resGetCategory)
    return res.status(500).json({ CODE: strings.BAD_REQUEST });

  //get persian category
  const oCategoryPersian = resGetCategory.catPersianTitle;
  console.log({ oCategoryPersian });

  //add owner to db
  const resAddOwner = await new Owner({
    oPhoneNumber,
    oOtherNumbers,
    oName,
    oAddress,
    oDistrict,
    oCategory,
    oCategoryPersian,
    oAvatar
  }).save();

  //log
  console.log({ resAddOwner });

  if (resAddOwner) {
    res.status(200).json({ msg: "Owner Created" });
  } else {
    res.status(500).json({ msg: "Failed" });
  }
});

router.put("/:ownerId", async (req, res, next) => {
  console.log("================ Admin Owner Edit ====================");
  //get id from url
  const _id = req.params.ownerId;
  console.log({ _id });
  //specify collection name
  const collectionName = "owners";
  console.log({ collectionName });

  //get params
  const params = req.body;
  console.log({ params });

  //ask python to update the collection ==> stringify the java script object and send it to python
  callPython(
    "editor.py",
    JSON.stringify({ collectionName, _id, params }),
    async result => {
      res.status(200).json({ CODE: result });
    }
  );
});

router.delete("/:ownerId", checkAdminAuth, async (req, res) => {
  const id = req.params.ownerId;
  const resDeleteOwner = await Owner.deleteOne({ _id: id });
  console.log({ resDeleteOwner });
  res.status(200).json({ CODE: strings.SUCCESS_DELETE });
});

module.exports = router;

// CODE: 1016 ==> Owner doesnt exists
