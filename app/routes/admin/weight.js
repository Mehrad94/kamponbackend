const express = require("express");
const router = express.Router();
const Category = require("app/models/category");
const mongoose = require("mongoose");

router.patch("/catsWeight", async (req, res) => {
  console.log("=============== Admin Category Weight Edit ================");
  console.log({ body: req.body });
  console.log({ headers: req.headers });
  console.log({ params: req.params });

  try {
    //get all keys
    const keys = Object.keys(req.body);
    console.log({ keys });

    //edit each categories weight
    for (let i = 0; i < keys.length; i++) {
      const resEditCategory = await Category.findOneAndUpdate(
        {
          _id: mongoose.Types.ObjectId(keys[i])
        },
        { weight: req.body[keys[i]] },
        { new: true }
      );
      console.log({ resEditCategory });
    }

    res.status(200).json({ CODE: 2024 });
  } catch (error) {
    console.log({ error });
    res.status(500).json({ ERROR: 1050 });
  }
});

module.exports = router;
