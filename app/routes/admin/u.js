const express = require("express");
const imgModel = require("app/models/image");
const router = express.Router();
const multer = require("multer");
const { SERVER } = require("app/values/strings");
const checkAdminAuth = require("app/middleware/checkAdminToken");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "application/vnd.android.package-archive"
  ) {
    cb(null, true);
  } else {
    cb("Error: Just .jpg and .jpeg files are allowed", false);
  }
};

const upload = multer({
  storage,
  fileFilter
});

router.post("/", checkAdminAuth, upload.single("image"), async (req, res) => {
  console.log("=============== Admin Uploading ===============");
  //log
  console.log({ file: req.file });

  //send the url to the client
  res.send({
    url: SERVER + req.file.path
  });
});

module.exports = router;
