const mongoose = require("mongoose");

const category = mongoose.Schema({
  catTitle: { type: String, required: true, unique: true },
  catPersianTitle: { type: String, required: true, unique: true },
  catImage: { type: String, unique: true },
  subCategories: [{ type: String }],
  activeCount: { type: Number, default: 0 },
  deactiveCount: { type: Number, default: 0 },
  weight: { type: Number, default: 1, unique: true }
});

module.exports = mongoose.model("Category", category);
