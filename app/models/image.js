const mongoose = require("mongoose");

const image = mongoose.Schema(
  {
    iName: { type: String, required: true },
    isUsed: { type: Boolean, default: false }
  },
  { timestamps: true }
);
module.exports = mongoose.model("Image", image);
