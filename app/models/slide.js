const mongoose = require("mongoose");

const slide = mongoose.Schema({
  sImage: { type: String, required: true },
  sType: { type: String, required: true },
  sDiscountId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Discount"
  },
  sOwner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Owner"
  },
});

module.exports = mongoose.model("Slide", slide);
