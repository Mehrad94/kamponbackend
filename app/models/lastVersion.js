const mongoose = require("mongoose");

const lastVersion = mongoose.Schema({
  memberAppVersion: { type: String },
  sellerAppVersion: { type: String }
});

module.exports = mongoose.model("LastVersion", lastVersion);
