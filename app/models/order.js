const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const order = mongoose.Schema({
  number: { type: Number },
  disId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Discount",
    required: true
  },
  isUsed: { type: Boolean, default: false },
  oMember: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Member",
    required: true
  },
  oOwner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Owner",
    required: true
  },
  oCount: { type: Number, default: 1 },
  oTitle: { type: String },
  oDescription: { type: String },
  oImages: [{ type: String }],
  oPercent: { type: Number },
  oRealPrice: { type: Number },
  oNewPrice: { type: Number },
  oTitle: { type: String },
  oCategory: { type: String },
  oCategoryPersian: { type: String },
  oDistrict: { type: String },
  oFeatures: [{ type: String }],
  oTermsOfUse: [{ type: String }],
  oIsGift: { type: Boolean, default: false },
  oAuthority: { type: String, required: true },
  trackCode: { type: String, required: true },
  time: { type: String }
});

order.index({ disTitle: "text" }, { weights: { name: 5 } });
mongoose.plugin(mongoosePaginate);

module.exports = mongoose.model("Order", order);
