const mongoose = require("mongoose");

const Link = mongoose.Schema({
  link: { type: String },
  title: { type: String }
});

module.exports = mongoose.model("Link", Link);
