const mongoose = require("mongoose");

const mLogin = mongoose.Schema(
  {
    mPhoneNumber: { type: String, require: true },
    mCode: { type: String }
  },
  { strict: false },
  { timestamps: true }
);

mLogin.index({ createdAt: 1 }, { expireAfterSeconds: 1 });

module.exports = mongoose.model("mLogin", mLogin);
