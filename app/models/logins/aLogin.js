const mongoose = require("mongoose");

const aLogin = mongoose.Schema(
  {
    aPhoneNumber: { type: String },
    aToken: { type: String },
    triedTimes: { type: Number, default: 0 }
  },
  { strict: false },
  { timestamps: true }
);

aLogin.index({ createdAt: 1 }, { expireAfterSeconds: 60 });

module.exports = mongoose.model("aLogin", aLogin);
