const mongoose = require("mongoose");

const oLogin = mongoose.Schema(
  {
    oPhoneNumber: { type: String },
    oToken: { type: String }
  },
  { strict: false },
  { timestamps: true }
);

oLogin.index({ createdAt: 1 }, { expireAfterSeconds: 1 });

module.exports = mongoose.model("oLogin", oLogin);
