const mongoose = require("mongoose");

const report = mongoose.Schema({
  disId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Discount",
    required: true
  },
  memberId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Member",
    required: true
  },
  time: { type: String },
  subjects: [{ type: String }],
  extraExplaination: { type: String }
});

module.exports = mongoose.model("Report", report);
