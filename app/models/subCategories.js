const mongoose = require("mongoose");

const subCategory = mongoose.Schema({
  category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
  subCatTitle: { type: String, required: true, unique: true },
  subCatPersianTitle: { type: String, required: true, unique: true },
  subCatImage: { type: String, unique: true },
  subActiveCount: { type: Number, default: 0 },
  subDeactiveCount: { type: Number, default: 0 },
  subWeight: { type: Number, default: 1 }
});

module.exports = mongoose.model("SubCategory", subCategory);
