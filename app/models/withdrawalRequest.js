const mongoose = require("mongoose");

const withdrawalRequest = mongoose.Schema(
  {
    owner: { type: mongoose.Schema.Types.ObjectId, ref: "Owner" },
    status: {
      type: String,
      enum: ["Waiting", "Accepted", "Rejected"],
      default: "Waiting"
    },
    time: { type: String },
    amount: { type: Number }
  },
  { timestamps: true }
);

module.exports = mongoose.model("WithdrawalRequest", withdrawalRequest);
