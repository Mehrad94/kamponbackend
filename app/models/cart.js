const mongoose = require("mongoose");

const cart = mongoose.Schema({
  cartBuyer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Member",
    required: true
  },
  cartDiscount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Discount",
    required: true
  },
  discountOwner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Owner",
    required: true
  },
  count: { type: Number, default: 0 },
  Authority: { type: String }
});

module.exports = mongoose.model("Cart", cart);
