const mongoose = require("mongoose");

const reportSubject = mongoose.Schema({
  subject: { type: String }
});

module.exports = mongoose.model("ReportSubject", reportSubject);
