const mongoose = require("mongoose");

const member = mongoose.Schema({
  mName: { type: String },
  mAddress: { type: String },
  mPhoneNumber: { type: String, required: true },
  mIsActive: { type: Boolean, default: true },
  isNewMember: { type: Boolean, default: true },
  mEmail: { type: String },
  mAvatar: {
    type: String,
    default: "https://www.kampon.ir:8443/uploads/defaultPictures/default.png"
  },
  bookmarks: [{ type: mongoose.Schema.Types.ObjectId, ref: "Discount" }]
});

module.exports = mongoose.model("Member", member);
