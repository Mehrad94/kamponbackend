const mongoose = require("mongoose");

const owner = mongoose.Schema(
  {
    oName: { type: String },
    oAddress: { type: String },
    oPhoneNumber: { type: String, required: true },
    oOtherNumbers: [{ type: String }],
    oDistrict: { type: String },
    cat: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category"
    },
    oCategory: { type: String },
    oCategoryPersian: { type: String },
    subCategories: [{ type: String }],
    oAvatar: { type: String },
    oIsActive: { type: Boolean, default: true },
    oBalance: { type: Number, default: 0 },
    oActiveKampon: { type: Number, default: 0 }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Owner", owner);
