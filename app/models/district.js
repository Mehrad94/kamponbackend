const mongoose = require("mongoose");

const district = mongoose.Schema({
  districtName: { type: String, unique: true }
});

module.exports = mongoose.model("District", district);
