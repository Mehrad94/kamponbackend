const mongoose = require("mongoose");

const TrackCode = mongoose.Schema({
  code: { type: String}
});

module.exports = mongoose.model("TrackCode", TrackCode);
