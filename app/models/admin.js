const mongoose = require("mongoose");

const admin = mongoose.Schema({
  aName: { type: String },
  aPhoneNumber: { type: String, required: true },
  aPassword: { type: String },
  aToken: { type: String }
});

module.exports = mongoose.model("Admin", admin);
