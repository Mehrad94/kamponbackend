const mongoose = require("mongoose");

const BlackList = mongoose.Schema({
  IP: { type: String, require = true },
  memberId: { type: mongoose.Schema.Types.ObjectId, ref: "Member" }
});


module.exports = mongoose.model('BlackList', BlackList)