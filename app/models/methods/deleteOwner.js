const mongoose = require("mongoose");
const owner = require("app/models/owner");
const discount = require("app/models/discount");
const slide = require("app/models/slide");

const deleteOwner = async _id => {
  //convert to object id
  _id = mongoose.Types.ObjectId(_id);

  //delete owner
  const resDeleteOwner = await owner.deleteOne({ _id });

  //delete discounts
  const resDeleteDiscounts = await discount.deleteMany({ ownerId: _id });

  //delete Slides
  const resDeleteSlides = await slide.deleteMany({ sOwner: _id });

  //
};
