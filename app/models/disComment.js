const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const disComment = mongoose.Schema(
  {
    cmtFrom: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Member",
      required: true
    },
    cmtText: { type: String, required: true },
    cmtReply: { text: { type: String }, time: { type: String } },
    cmtTo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Discount",
      required: true
    },
    cmtStatus: {
      type: String,
      enum: ["Waiting", "Accepted", "Rejected", "Deleted"],
      default: "Waiting"
    },
    cmtTime: { type: String },
    cmtRating: { type: Number, required: true }
  },
  { timestamps: true }
);

mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("DisComment", disComment);
