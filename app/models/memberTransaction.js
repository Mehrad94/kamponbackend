const mongoose = require("mongoose");

const memberTransaction = mongoose.Schema(
  {
    mtAmount: { type: Number, required: true },
    mtDescription: { type: String },
    mtAuthority: { type: String, required: true },
    mtOwner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Owner",
      required: true
    },
    mtTime: { type: String }
  },
  { timestamps: true }
);

module.exports = mongoose.model("MemberTransaction", memberTransaction);
