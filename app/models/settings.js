const mongoose = require("mongoose");

const Settings = mongoose.Schema(
  {
    sLinks: {
      sellerApplication: { type: String, default: "" },
      sellerBazar: { type: String, default: "" },
      memberApplication: { type: String, default: "" },
      memberBazar: { type: String, default: "" },
      backToAppLink: { type: String, default: "" },
      facebook: { type: String, default: "" },
      twitter: { type: String, default: "" },
      instagram: { type: String, default: "" },
      telegram: { type: String, default: "" }
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Settings", Settings);
