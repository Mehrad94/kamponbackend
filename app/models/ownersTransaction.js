const mongoose = require("mongoose");

const ownersTransaction = mongoose.Schema(
  {
    otType: {
      type: String,
      enum: ["DEPOSIT", "WITHDRAWAL"],
      default: "DEPOSIT"
    },
    otAmount: { type: Number, required: true },
    otDescription: { type: String },
    otAuthority: { type: String },
    otOwner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Owner",
      required: true
    },
    otTime: { type: String }
  },
  { timestamps: true }
);

module.exports = mongoose.model("ownersTransaction", ownersTransaction);
