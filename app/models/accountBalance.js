const mongoose = require("mongoose");

const accountBalance = mongoose.Schema({
  amount: { type: Number, required: true, default: 0 },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: "Owner" },
  lastModified: { type: String }
});

module.exports = mongoose.model("accountBalance", accountBalance);
