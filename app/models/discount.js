const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const discount = mongoose.Schema(
  {
    disTitle: { type: String, text: true },
    disDescription: { type: String },
    disImages: [{ type: String }],
    disThumbnail: { type: String },
    disAddress: { type: String },
    disPercent: { type: String },
    disRealPrice: { type: Number, required: true },
    disNewPrice: { type: Number, required: true },
    subCategory: { type: String },
    disCategory: { type: String, required: true },
    disCategoryPersian: { type: String, required: true },
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Owner",
      required: true
    },
    disDistrict: { type: String },
    disExpireDate: { type: String },
    disFeatures: [{ type: String }],
    disTermsOfUse: [{ type: String }],
    isActive: { type: Boolean, default: true },
    boughtCount: { type: Number, default: 0 },
    rating: { type: Number },
    viewCount: { type: Number, default: 0 }
  },
  { timestamps: true }
);

discount.index({ disTitle: "text" }, { weights: { name: 5 } });
mongoose.plugin(mongoosePaginate);
module.exports = mongoose.model("Discount", discount);
