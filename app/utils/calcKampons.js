const Discount = require("app/models/discount");
const mongoose = require("mongoose");

const calcKampons = async (ownerId, isActive) => {
  const resGetDiscounts = await Discount.find({
    ownerId: mongoose.Types.ObjectId(ownerId),
    isActive
  });

  return resGetDiscounts;
};

module.exports = calcKampons;
