const Order = require("app/models/order");
const mongoose = require("mongoose");

const calcOrders = async (ownerId, isUsed) => {
  const resGetOrders = await Order.find({
    oOwner: mongoose.Types.ObjectId(ownerId),
    isUsed
  });

  console.log({ resGetOrders });
  return resGetOrders;
};

module.exports = calcOrders;
