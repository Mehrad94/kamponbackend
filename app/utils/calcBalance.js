const ownersTransaction = require("app/models/ownersTransaction");
const mongoose = require("mongoose");

const calcBalance = async ownerId => {
  let balance = 0;
  const transactions = await ownersTransaction.find({
    otOwner: mongoose.Types.ObjectId(ownerId)
  });

  transactions.map(tr => {
    balance += tr.otAmount;
  });

  return balance;
};

module.exports = calcBalance;
