const Owner = require("app/models/owner");
const Discount = require("app/models/discount");
const mongoose = require("mongoose");
const calcActiveDiscount = async () => {
  console.log("================== Calc Active Discount ==================");
  const resGetOwner = await Owner.find();
  for (let i = 0; i < resGetOwner.length; i++) {
    const ownerId = resGetOwner[i]._id;
    const resGetAllDiscountByOwnerId = await Discount.find({ ownerId });
    if (resGetAllDiscountByOwnerId.length === 0) console.log("Salam");
    console.log("=====");
  }
};

module.exports = calcActiveDiscount;
