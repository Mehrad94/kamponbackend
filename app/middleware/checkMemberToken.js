const jwt = require("jsonwebtoken");
const securityConstants = require("app/constants/Security");
const express = require("express");
const router = express.Router();
const Member = require("app/models/member");
const mongoose = require("mongoose");

module.exports = async (req, res, next) => {
  try {
    console.log("============ Member Check Token MiddleWare==============");
    console.log({ token: req.headers.authorization });

    //get token from header
    const token = req.headers.authorization.split(" ")[1];

    //verify token and get mPhoneNumber from it
    const tokenInfo = jwt.verify(token, securityConstants.tokensKey);

    //set mPhoneNumber in req.tokenInfo
    req.tokenInfo = tokenInfo;

    console.log({ tokenInfo });

    //find this member from database
    const resGetMember = await Member.findOne({
      _id: mongoose.Types.ObjectId(tokenInfo._id),
      mIsActive: true
    });

    //if this member doesnt exists
    if (!resGetMember) {
      console.log({ ERROR: 1012 });
      return res.status(500).json({ ERROR: 1012 });
    }

    //log
    console.log("next()");

    //go to the next level
    next();
  } catch (e) {
    console.log({ e });
    res.status(500).json({ ERROR: 1012 });
  }
};
