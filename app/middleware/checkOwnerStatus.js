const jwt = require("jsonwebtoken");
const Security = require("app/constants/Security");
const Owner = require("app/models/owner");
module.exports = async (req, res, next) => {
  console.log("\n============== Owner Token Middleware  ==============");
  try {
    const token = req.headers.authorization.split(" ")[1];
    const tokenInfo = jwt.verify(token, Security.tokensKey);
    console.log({ tokenInfo });
    const { _id } = tokenInfo;
    const resGetOwner = await Owner.findOne({ _id });
    console.log({ resGetOwner });

    if (
      !tokenInfo.isAdmin &&
      tokenInfo.isOwner &&
      !tokenInfo.isMember &&
      resGetOwner.oIsActive
    ) {
      req.tokenInfo = tokenInfo;
      next();
    } else res.status(400).json({ ERROR: 1022 });
  } catch (e) {
    res.status(400).json({ ERROR: 1022 });
  }
};
