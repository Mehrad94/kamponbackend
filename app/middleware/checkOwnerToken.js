const jwt = require("jsonwebtoken");
const Security = require("app/constants/Security");
const Owner = require("app/models/owner");
module.exports = async (req, res, next) => {
  console.log("\n============= Check Token Route ================");
  console.log({ Header: req.headers });
  try {
    const token = req.headers.authorization.split(" ")[1];
    const tokenInfo = jwt.verify(token, Security.tokensKey);
    const { _id } = tokenInfo;
    const resGetOwner = await Owner.findOne({ _id });
    if (resGetOwner.isActive) {
      if (!tokenInfo.isAdmin && !tokenInfo.isOwner && tokenInfo.isMember)
        next();
      else res.status(400).json({ ERROR: 1022 });
    } else res.status(400).json({ ERROR: 1022 });
    req.tokenInfo = tokenInfo;
  } catch (e) {
    res.status(400).json({ ERROR: 1022 });
  }
};
