const jwt = require("jsonwebtoken");
const Security = require("app/constants/Security");

module.exports = async (req, res, next) => {
  try {
    console.log(
      "\n============================== Admin Check Token Middleware =============================="
    );

    const token = req.headers.authorization.split(" ")[1];
    console.log({ token: req.headers.authorization });
    const tokenInfo = jwt.verify(token, Security.tokensKey);
    req.tokenInfo = tokenInfo;
    console.log({ tokenInfo });
    if (tokenInfo.isAdmin && !tokenInfo.isOwner && !tokenInfo.isMember) {
      next();
    } else {
      res.status(400).json({ CODE: 1022 });
    }
  } catch (e) {
    res.status(500).json({ CODE: 1022 });
  }
};
