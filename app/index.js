const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
var Kavenegar = require("kavenegar");
const c = require("app/routes/c");
const d = require("app/routes/d");
const u = require("app/routes/u");
const o = require("app/routes/o");
const s = require("app/routes/s");
const m = require("app/routes/m");
const a = require("app/routes/a");
const b = require("app/routes/b");
const or = require("app/routes/or");
const search = require("app/routes/search");
const home = require("app/routes/home");
const special = require("app/routes/special");
const bookmark = require("app/routes/bookmark");
const cart = require("app/routes/cart");
const checkToken = require("app/routes/checkToken");
const checkIfDiscountExistsInCart = require("app/routes/checkIfDiscountExistsInCart");
const order = require("app/routes/order");
const getBankURL = require("app/routes/getBankUrl");
// const paymentDone = require("app/routes/paymentDone");
const addAdmin = require("app/routes/admin/addAdmin");
const owner = require("app/routes/admin/owner");
const cleanUp = require("app/routes/admin/cleaner");
const getKampons = require("app/routes/member/getKampons");
const accountBalance = require("app/routes/owner/accountBalance");
const link = require("app/routes/admin/link");
const mGetCode = require("app/routes/member/getCode");
const mVerifyCode = require("app/routes/member/verifyCode");
const settings = require("app/routes/admin/settings");
const mProfile = require("app/routes/member/profile");
const report = require("app/routes/member/report");
const adminReport = require("app/routes/admin/report");
const adminReportSubjects = require("app/routes/admin/reportSubject");
const memberReportSubjects = require("app/routes/member/reportSubject");
const memberCheckUpdate = require("app/routes/member/checkUpdate");

const adminGift = require("app/routes/admin/gift");

//Admin Routes
const aMember = require("app/routes/admin/aMember");
const aDiscount = require("app/routes/admin/aDiscount");
// const aLogin = require("app/routes/admin/aLogin");
const aGetCode = require("app/routes/admin/getCode");
const ac = require("app/routes/admin/c");
const ad = require("app/routes/admin/d");
const as = require("app/routes/admin/s");
const ao = require("app/routes/admin/o");
const au = require("app/routes/admin/u");
const aVerifyCode = require("app/routes/admin/verifyCode");
const ownerSearch = require("app/routes/admin/ownerSearch");
const memberSearch = require("app/routes/admin/memberSearch");
const replyWithdrawal = require("app/routes/admin/replyWithdrawal");
const getWithdrawals = require("app/routes/admin/getWithdrawals");
const activeOrDeactiveDiscounts = require("app/routes/admin/activeOrDeactiveDiscounts");
const date = require("app/routes/date");
const catsWeight = require("app/routes/admin/weight");

//Owner Routes
const getCode = require("app/routes/owner/getCode");
const verifyCode = require("app/routes/owner/verifyCode");
const checkOrder = require("app/routes/owner/checkIfQRCodeIsValid");
const checkTrackCode = require("app/routes/owner/checkIfTrackCodeIsValid");

const isUsed = require("app/routes/owner/isUsed");
const getActiveDiscount = require("app/routes/owner/getActiveDiscounts");
const getDeactiveDiscount = require("app/routes/owner/getDeactivedDiscounts");
const oCheckToken = require("app/routes/owner/checkToken");
const getUsed = require("app/routes/owner/getUsed");
const getNotUsed = require("app/routes/owner/getNotUsed");
const transactions = require("app/routes/owner/getTransaction");
const ownerInfo = require("app/routes/owner/info");
const withdrawalRequest = require("app/routes/owner/withdrawalRequest");
const district = require("app/routes/admin/district");
const memberDisComment = require("app/routes/member/comment");
const adminDisComment = require("app/routes/admin/comment");

const fakeOrder = require("app/routes/fakeOrder");
const trackCode = require("app/routes/trackCode");

var index = express();

var api = Kavenegar.KavenegarApi({
  apikey:
    "44503346776A7533366C6A652F783963704B477673427650684B6A39526A50363371357A6E38695865486F3D"
});

//---------------- Connecting to Mongoose
//_df54DFGD65x_cvDDZ656
//mongo --host 188.212.22.88 -u root -p _df54DFGD65x_cvDDZ656 --authenticationDatabase admin

mongoose
  .connect(
    "mongodb://root:_df54DFGD65x_cvDDZ656@188.212.22.88:27017/kamponDB",
    {
      useNewUrlParser: true,
      authSource: "admin"
    }
  )
  .then(res => {
    console.log("Mongoose conected!!!");
  })
  .catch(err => {
    console.log({ err });
  });
mongoose.set("useFindAndModify", false);

//Express Options
index.use("/uploads", express.static("uploads"));
index.use(bodyParser.urlencoded({ extended: false }));
index.use(bodyParser.json());

//Handling CORS Error
index.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Reuested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

//  Routes which should handle request
index.use("/c", c);
index.use("/d", d);
index.use("/member/u", u);
index.use("/o", o);
index.use("/s", s);
index.use("/m", m);
index.use("/a", a);
index.use("/b", b);
index.use("/or", or);
index.use("/search", search);
index.use("/special", special);
index.use("/bookmark", bookmark);

//  Routes which should handle request
index.use("/admin/member", aMember);
index.use("/admin/discount", aDiscount);
// index.use("/admin/login", aLogin);
index.use("/admin/c", ac);
index.use("/admin/d", ad);
index.use("/admin/s", as);
index.use("/admin/o", ao);
index.use("/admin/u", au);
index.use("/admin/cleanUp", cleanUp);
index.use("/home", home);
index.use("/cart", cart);
index.use("/checkToken", checkToken);
index.use("/checkCart", checkIfDiscountExistsInCart);
index.use("/order", order);
index.use("/fakeOrder", fakeOrder);
index.use("/owner/getCode", getCode);
index.use("/owner/verifyCode", verifyCode);
index.use("/owner/checkOrder", checkOrder);
index.use("/owner/checkTrackCode", checkTrackCode);
index.use("/owner/isUsed", isUsed);
index.use("/admin/getCode", aGetCode);
index.use("/admin/verifyCode", aVerifyCode);
index.use("/getBankURL", getBankURL);
// index.use("/paymentDone", paymentDone);
index.use("/owner/getActives", getActiveDiscount);
index.use("/owner/getDeactives", getDeactiveDiscount);
index.use("/admin/addAdmin", addAdmin);
index.use("/admin/owner", owner);
index.use("/owner/checkToken", oCheckToken);
index.use("/admin/ownerSearch", ownerSearch);
index.use("/admin/memberSearch", memberSearch);
index.use("/owner/getUsed", getUsed);
index.use("/owner/getNotUsed", getNotUsed);
index.use("/owner/transactions", transactions);
index.use("/member/kampons", getKampons);
index.use("/owner/accountBalance", accountBalance);
index.use("/owner/info", ownerInfo);
index.use("/owner/withdrawalRequest", withdrawalRequest);
index.use("/admin/cashOut", replyWithdrawal);
index.use("/admin/withdrawals", getWithdrawals);
index.use("/admin/deactiveDiscount", activeOrDeactiveDiscounts);
index.use("/admin/district", district);
index.use("/admin/link", link);
index.use("/member/getCode", mGetCode);
index.use("/member/verifyCode", mVerifyCode);
index.use("/admin/settings", settings);
index.use("/member/profile", mProfile);
index.use("/member/report", report);
index.use("/trackCode", trackCode);
index.use("/date", date);
index.use("/d/comment", memberDisComment);
index.use("/admin/comment", adminDisComment);
index.use("/admin/report", adminReport);
index.use("/admin/reportSubjects", adminReportSubjects);
index.use("/member/reportSubjects", memberReportSubjects);
index.use("/admin/gift", adminGift);
index.use("/member/checkUpdate", memberCheckUpdate);
index.use("/admin/weight", catsWeight);

module.exports = index;
