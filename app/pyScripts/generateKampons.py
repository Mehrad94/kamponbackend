from database import orders
import sys, json
from bson.objectid import ObjectId

# define a function to remove a key from a dictionary beacase we want to delete objectIds from orderDictionaries to be able to json stringify it
def delFromDictionary(dictionary, *key):
    # add order string
    dictionary["orderString"] = str(dictionary["oMember"]) + str(dictionary["oOwner"]) +  str(dictionary["disId"]) + str(dictionary["number"])
    
    # delete keys
    for k in key:
        dictionary[k] = str(dictionary[k])
    return dictionary

try:
    # get memberId from NodeJS
    memberId = ObjectId(sys.argv[1])
    # memberId = ObjectId("5d5abb09d16fba66c82b894e")
    
    # get all orders of this member from database
    resGetOrders = orders.find({"oMember": memberId})

    # make a list of orders 
    resGetOrders = list(resGetOrders)

    # generate a list of order strings and order objects
    kampons = [{"kampon": delFromDictionary(order,"oMember","oOwner", "disId", "_id" )} for order in resGetOrders ]

    # json stringify the dictionary and send the result to nodeJs
    print("\n",json.dumps(kampons))

except Exception as ex:
    print(ex)