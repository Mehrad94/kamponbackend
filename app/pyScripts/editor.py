import sys, json
from database import kamponDB
from bson.objectid import ObjectId

try:
    # get the editing information ==> convert json string to dictionary 
    jsonObject = json.loads(sys.argv[1])

    # get collection name
    collectionName = jsonObject["collectionName"]

    # get collection
    collection = kamponDB[collectionName]

    # get id of the fieled that should be deleted
    _id = ObjectId(jsonObject["_id"])
    # _id = ObjectId("5d4554cac64915159847101a")

    # get parameters
    params = jsonObject["params"]

    # delete _id
    if "_id" in params:
        del params["_id"]

    if "ownerId" in params:
        params["ownerId"] = ObjectId(params["ownerId"])
    # update
    resUpdate = collection.update_one({"_id": _id},{"$set": params})

    # send the result to nodeJs
    print("Done")
except:
    print("Failed")
