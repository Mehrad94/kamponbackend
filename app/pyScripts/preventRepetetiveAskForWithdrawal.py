from database import withdrawalRequests
import sys
from bson.objectid import ObjectId


try:

    # get ownerId from nodeJS
    ownerId = sys.argv[1]


    # get all withdrawal requests of this owner sorted by created time || type == mongoCursor 
    allWithdrawalRequests = withdrawalRequests.find({"owner" :  ObjectId(ownerId)}).sort("createdAt", -1)


    # make a list of withdrawal requests 
    allWithdrawalRequests = list(allWithdrawalRequests)


    # check if the last request he made is in waiting mode or not and pass the result to node, "0" => invalid request, "1" => valid request
    print("0") if len(allWithdrawalRequests) and allWithdrawalRequests[0]['status'] == "Waiting" else print("1")


except:
    print("0")
