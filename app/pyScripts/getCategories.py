from database import categories, discounts, owners
import json, pymongo

def calcActiveAndDeactiveCounts(catItem):
    resGetOwners = list(owners.find({"oCategory" : catItem["catTitle"]}))
    resGetActiveDiscounts = list(discounts.find({"disCategory": catItem["catTitle"], "isActive": True}) )
    resGetDeactiveDiscounts = list(discounts.find({"disCategory": catItem["catTitle"], "isActive": False}))
    catItem["owners"], catItem["activeCount"], catItem["deactiveCount"] = len(resGetOwners), len(resGetActiveDiscounts), len(resGetDeactiveDiscounts)
    catItem["_id"] = str(catItem["_id"])

    return catItem


def deleteNone(item):
    if item != None:
        return True
    else:
        return False


try:

    resGetCategories = list(categories.find().sort([("weight", pymongo.ASCENDING)]))
    modifiedCategories = list(map(calcActiveAndDeactiveCounts, resGetCategories))

    print(json.dumps(modifiedCategories))

except Exception as e:
    print(e)