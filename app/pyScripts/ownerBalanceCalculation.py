import sys
from database import ownersTransactions as ot
from bson.objectid import ObjectId

try:
    # get ownerId from nodeJS
    ownerId = sys.argv[1]

    # initialize balance
    balance = 0

    # get all transactions of this owner ==> returned type: mongoCursor
    transActions = ot.find({"otOwner" : ObjectId(ownerId)})

    # make a list of all transactions
    transActions = list(transActions)

    # calculate balance
    for transaction in transActions:
        balance += transaction["otAmount"]

    # pass the result to nodeJS
    print(balance)
except:
    print("0")