import sys, json
from database import slides
from bson.objectid import ObjectId
 
try:
    # get sType from nodeJS
    sType = sys.argv[1]

    # get all slides with this type ==> returned type: mongoCursor
    resGetAllSlides = slides.find({"sType" : sType})

    # make a list of all slides
    resGetAllSlides = list(resGetAllSlides)

    # add field number to each retrieved slide
    for index, slide in enumerate(resGetAllSlides):
       
        # add slide number as slideId for front end guyes starting from 1
        slide["slideId"] = index + 1

        # convert objectId's to string id's in one line ;)
        slide["_id"], slide["sOwner"], slide["sDiscountId"] = str(slide["_id"]), str(slide["sOwner"]), str(slide["sDiscountId"])

    # make a json output and pass it to nodeJS
    print(json.dumps(resGetAllSlides))
except:
    print([])
