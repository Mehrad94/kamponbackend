const constants = require("./Strings");
const request = require("request-promise");

const sendLoginSMS = async (receptor, token, res) => {
  console.log(
    "\n====================== Send SMS To " +
      receptor +
      " ====================== "
  );
  //get constants
  const SENDER = constants.SENDER_NUMBER;
  const API_Key = constants.SMS_API_KEY;
  const TEMPLATE_NAME = constants.LOGIN_SMS_PATTERN_NAME;

  //prepare url
  const URL =
    "https://api.kavenegar.com/v1/" +
    API_Key +
    "/verify/lookup.json?receptor=" +
    receptor +
    " &token=" +
    token +
    "&template=" +
    TEMPLATE_NAME;

  //log
  console.log({ SENDER, API_Key, URL, TEMPLATE_NAME });

  //make option object
  let options = {
    method: "GET",
    uri: URL,
    headers: {
      "cache-control": "no-cache",
      "content-type": "aplication/json"
    },
    json: true
  };

  //send request
  request(options).then(async data => {
    //log
    console.log({ status: data.return.status });
    //200 means its send
    if (data.return.status == 200) {
      //log
      //send result to the client
      if (isNewMember === true) {
        //log
        console.log({ CODE: 2029, isNewMember: true });
        res.status(200).json({ CODE: 2029, isNewMember: true });
      } else if (isNewMember === false) {
        console.log({ CODE: 2029, isNewMember: false });
        res.status(200).json({ CODE: 2029, isNewMember: false });
      } else {
        console.log({ CODE: 2029 });
        res.status(200).json({ CODE: 2029 });
      }
    } else {
      //log
      console.log({ ERROR: 1029 });

      //send result to the client
      res.status(500).json({ ERROR: 1029 });
    }
  });
};

const sendWidthdrawalSMS = async (token, token2, token3, res) => {
  console.log(
    "\n====================== Sending Withdrawal Requst Notification "
  );
  //get constants
  const SENDER = constants.SENDER_NUMBER;
  const API_Key = constants.SMS_API_KEY;
  const TEMPLATE_NAME = constants.WIDTHDRAWAL_SMS_PATTERN_NAME;
  const receptor = "09379514786";

  //prepare url
  const URL =
    "https://api.kavenegar.com/v1/" +
    API_Key +
    "/verify/lookup.json?receptor=" +
    receptor +
    " &token=" +
    token +
    "&token2=" +
    token2 +
    "&token3=" +
    token3 +
    "&template=" +
    TEMPLATE_NAME;

  //log
  console.log({ SENDER, API_Key, URL, TEMPLATE_NAME });

  //make option object
  let options = {
    method: "GET",
    uri: URL,
    headers: {
      "cache-control": "no-cache",
      "content-type": "aplication/json"
    },
    json: true
  };

  //send request
  request(options).then(async data => {
    //log
    console.log({ status: data.return.status });
    //200 means its send
    if (data.return.status == 200) {
      console.log({ CODE: 2029 });
      res.status(200).json({ CODE: 2029 });
    } else {
      //log
      console.log({ ERROR: 1029 });

      //send result to the client
      res.status(500).json({ ERROR: 1029 });
    }
  });
};

module.exports = { sendLoginSMS, sendWidthdrawalSMS };
