var moment = require("jalali-moment");

 const getTime = () => {
  //get current date
  const persianDate = moment()
    .locale("fa")
    .format("YYYY/M/D");

  //get current clock
  var today = new Date();
  var clock = today.getHours() + ":" + today.getMinutes();

  //make final time
  const finalTime = persianDate + " " + clock;

  //retuen final time
  return finalTime;
};

module.exports = getTime;
