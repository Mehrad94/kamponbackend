const jwt = require("jsonwebtoken");
const tokensKey = "SECRET";
const Joi = require("@hapi/joi");
var ObjectId = require("mongoose").Types.ObjectId;
var bcrypt = require("bcryptjs");
const Cryptr = require("cryptr");
const getTime = require("./getTime");

// Joi Schema

const schema = Joi.object().keys({
  // username: Joi.string().alphanum().min(3).max(30).required(),
  // password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
  // access_token: [Joi.string(), Joi.number()],
  // birthyear: Joi.number().integer().min(1900).max(2013),
  // email: Joi.string().email({ minDomainSegments: 2 })

  category: Joi.string()
    .alphanum()
    .min(3)
    .max(50),
  pageNumber: Joi.number()
    .min(1)
    .max(999)
});

validate = anObjectThatShoulBeValidated => {
  const result = Joi.validate(anObjectThatShoulBeValidated, schema);
  return result;
};

validateMongooseId = mongooseId => {
  return ObjectId.isValid(mongooseId);
};

generateRandomString = () => {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < 6; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

generateTrackCode = () => {
  let time = new Date();
  var code = time.getHours() + time.getMinutes() + time.getMilliseconds();
  return code;
};
isHashValid = async (pass, dbHash) => {
  return await bcrypt.compareSync(pass, dbHash);
};

hashIt = async clearText => {
  var hash = await bcrypt.hashSync(clearText, 8);
  return hash;
};

encrypt = async (clearText, key) => {
  const cryptr = new Cryptr(key);
  const encryptedString = await cryptr.encrypt(clearText);
  return encryptedString;
};

decrypt = async (encryptedString, key) => {
  const cryptr = new Cryptr(key);
  const decryptedString = await cryptr.decrypt(encryptedString);
  return decryptedString;
};

generateRandomNumber = length => {
  const digits = "0123456789";
  let number = "";
  for (let i = 1; i <= length; i++) {
    let index = Math.floor(Math.random() * 10);
    number += digits.charAt(index);
  }

  return number;
};

IDGenerator = () => {
  console.log("================== ID ==============");
  this.length = 6;
  this.timestamp = +new Date();

  var _getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  this.generate = function() {
    var ts = this.timestamp.toString();
    var parts = ts.split("").reverse();
    var id = "";

    for (var i = 0; i < this.length; ++i) {
      var index = _getRandomInt(0, parts.length - 1);
      id += parts[index];
    }

    console.log({ id });
    return id;
  };
  return this.generate();
};
const exprt = {
  validate,
  tokensKey,
  validateMongooseId,
  hashIt,
  generateRandomString,
  generateRandomNumber,
  generateTrackCode,
  IDGenerator,
  encrypt,
  decrypt
};

module.exports = exprt;
