var spawn = require("child_process").spawn;

const callPython = (scriptName, args, callBack) => {
  var path = __dirname + "/../pyScripts/" + scriptName;
  // var path = "E:\\Dev\\KampayApi\\app\\constants\\..\\pyScripts\" + scriptName;

  console.log({ scriptName, args, path });
  var process = spawn("python3", [path, args]);

  process.stdout.on("data", data => {
    result = data.toString();
    callBack(result);
  });

  process.stderr.on("data", function(data) {
    console.log("stderr: " + data);
  });

  process.on("close", function(code) {
    console.log("child process exited with code " + code);
  });
};

module.exports = callPython;
