const MOST_DISCOUNT = "mostDiscount";
const POPULAR = "popular";
const NEWEST = "newest";
const OUR_OFFER = "ourOffer";
const MOST_SOLD = "mostSold";
const SERVER = "https://www.kampon.ir:8443/";

//============================= SUCCESS ===============================
const SUCCESS_ADD = 2084;
const SUCCESS_DELETE = 2083;
const SUCCESS_ADD_LINK = 2097;
const SUCCESS_SMS_SENT = 2079;
const SUCCESS_UPDATED = 2013;

//============================= Bad Request ===============================
const BAD_REQUEST = 5010;
const DISCOUNT_NOT_FOUND = 5011;
const INVALID_OWNER = 5067;
const string = {
  MOST_DISCOUNT,
  SUCCESS_ADD,
  SUCCESS_DELETE,
  POPULAR,
  NEWEST,
  OUR_OFFER,
  MOST_SOLD,
  BAD_REQUEST,
  SUCCESS_ADD_LINK,
  SUCCESS_SMS_SENT,
  SUCCESS_UPDATED,
  INVALID_OWNER,
  SERVER
};

module.exports = string;
